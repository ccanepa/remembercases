
E:\_repos\_conv\pg_wk>rem para correr hg instalado en python sin poner en el paths a Scripts 

E:\_repos\_conv\pg_wk>c:\python27-64\scripts\hg.exe log         
changeset:   45:4d0afd8e98f9
bookmark:    master
tag:         tip
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Sat May 13 11:50:10 2017 -0300
files:       remembercases/cmd_timeout.py remembercases/proxy.py remembercases/test/test_cmd_timeout.py
description:
better cmd timeout for py3


changeset:   44:f1a9c9396726
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Mon May 08 02:53:31 2017 -0300
files:       remembercases/samples/compare_snapdirs.py
description:
compare snapdirs now only target *.png


changeset:   43:5b7027ed588b
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Mon May 08 02:49:50 2017 -0300
files:       remembercases/image_comparison.py remembercases/samples/list_black_snapshoots.py
description:
added script to detect snapshoots totally black


changeset:   42:56528326fae9
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Sun Mar 19 12:17:34 2017 -0300
files:       remembercases/cmd_timeout.py remembercases/db.py remembercases/doers.py remembercases/image_comparison.py remembercases/proxy.py remembercases/snapshot_taker.py
description:
pep8 changes


changeset:   41:e0663ad389ac
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Sun Mar 19 10:22:16 2017 -0300
files:       README
description:
updated readme


changeset:   40:05134e45dc90
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Sat Mar 18 16:28:06 2017 -0300
files:       CHANGELOG remembercases/samples/compare_snapdirs.py
description:
added compare_snapdirs.py to report diferences between two directories with images


changeset:   39:77b1278ce972
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Sat Mar 18 16:15:36 2017 -0300
files:       CHANGELOG remembercases/__init__.py remembercases/db.py remembercases/doers.py remembercases/image_comparison.py remembercases/proxy.py remembercases/snapshot_taker.py remembercases/test/test_doers.py
description:
code runs in py2 and py3


changeset:   38:5c59139685c1
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Sat Mar 18 15:57:40 2017 -0300
files:       CHANGELOG
description:
updates changelog


changeset:   37:d4cbf3d94242
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Sat Mar 18 15:47:27 2017 -0300
files:       .hgtags
description:
tagging version 0.1 (py2x only)


changeset:   36:c830e6c35126
tag:         v0.1
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Sat Mar 18 15:46:09 2017 -0300
files:       .hgignore remembercases/__init__.py
description:
adding version


changeset:   35:69e687abf279
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Sat Mar 08 13:25:56 2014 -0300
files:       remembercases/test/test_cmd_timeout.py remembercases/test/test_proxy.py
description:
cleanup


changeset:   34:f7725a48688d
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Sat Mar 08 13:15:51 2014 -0300
files:       remembercases/cmd_timeout.py
description:
clean some u unicode prefix


changeset:   33:f6db972ca82d
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Fri Mar 07 03:17:42 2014 -0300
files:       remembercases/cmd_timeout.py remembercases/test/cmd_timeout_test_helper.py remembercases/test/test_cmd_timeout.py
description:
cmd_timeout runner now always return unicode - other scripts may need adjust


changeset:   32:6f259bd044d7
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Thu Mar 06 11:30:48 2014 -0300
files:       remembercases/test/cmd_timeout__explore_output_capture.py remembercases/test/cmd_timeout_explore_communicate_encoding.py remembercases/test/cmd_timeout_proxy_encoding.py remembercases/test/cmd_timeout_target_encoding.py remembercases/test/explore_cmd_timeout__output_capture.py
description:
add scripts to explore subprocess communicate


changeset:   31:2ddec34e19ec
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Thu Mar 06 11:17:32 2014 -0300
files:       remembercases/test/explore_cmd_timeout.py remembercases/test/explore_cmd_timeout__output_capture.py
description:
better script name


changeset:   30:92dacb34d32c
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Wed Mar 05 15:18:46 2014 -0300
files:       remembercases/cmd_timeout.py remembercases/test/explore_cmd_timeout.py remembercases/test/test_cmd_timeout.py
description:
reverted decoding in cmd_timeout.py


changeset:   29:907913ec7a73
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Thu Feb 27 16:23:54 2014 -0300
files:       remembercases/db.py
description:
set pickle protocol to 2 for py2 - py3 compatibility


changeset:   28:731da94b9eb0
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Sun Feb 23 13:46:31 2014 -0300
files:       CHANGELOG remembercases/image_comparison.py
description:
compatibility with both PIL and Pillow, issue #2


changeset:   27:28b5aef2e4ba
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Fri Jan 10 17:04:05 2014 -0300
files:       remembercases/cmd_timeout.py
description:
testsuite runs green both in 2 and 3, but cmd output captured as str; desirable would be unicode allways


changeset:   26:bd95bf1bd258
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Fri Jan 10 16:12:39 2014 -0300
files:       remembercases/db.py remembercases/proxy.py remembercases/test/readme.txt remembercases/test/test_cmd_timeout.py remembercases/test/test_db.py
description:
some more py3 compatibility


changeset:   25:99b5150c13ab
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Sat Sep 08 23:49:46 2012 -0300
files:       remembercases/db.py remembercases/doers.py remembercases/image_comparison.py remembercases/proxy.py remembercases/snapshot_taker.py remembercases/test/dummy_proxy.py remembercases/test/test_ScreenSampler_parse_fullcommand.py remembercases/test/test_ScreenSampler_sampler.py remembercases/test/test_db.py remembercases/test/test_doers.py remembercases/test/test_proxy.py
description:
first steps toward python3 compatibility


changeset:   24:afa7b2314773
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Wed Jul 04 16:02:28 2012 -0300
files:       remembercases/doers.py remembercases/image_comparison.py remembercases/test/test_doers.py
description:
added doers.join_sorted


changeset:   23:666559adb3e7
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Mon Jul 02 12:49:50 2012 -0300
files:       remembercases/db.py remembercases/test/test_db.py
description:
db: fix del_testbed, make set_default_testbed return the old testbed


changeset:   22:e01ed86bef31
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Sun Jul 01 19:04:39 2012 -0300
files:       remembercases/db.py remembercases/test/test_db.py
description:
added db.clone_testbed and db.del_testbed


changeset:   21:59a28763a3b3
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Thu Jun 21 14:42:27 2012 -0300
files:       remembercases/db.py remembercases/test/test_db.py
description:
added db.del_entity


changeset:   20:3e5ea64ba7ca
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Wed Jun 20 14:16:01 2012 -0300
files:       remembercases/snapshot_taker.py
description:
fix typo in snapshot_taker, deleted debug prints


changeset:   19:ecbbcd9085ec
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Wed Jun 20 13:37:08 2012 -0300
files:       remembercases/snapshot_taker.py remembercases/test/test_ScreenSampler_parse_fullcommand.py remembercases/test/test_ScreenSampler_sampler.py
description:
eliminated testinfo kr and ks commands


changeset:   18:c8efd5ff6a70
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Wed Jun 20 13:30:37 2012 -0300
files:       remembercases/snapshot_taker.py remembercases/test/test_ScreenSampler_parse_fullcommand.py remembercases/test/test_ScreenSampler_sampler.py
description:
added testinfo commands 'f n dt' :repeat dt s, 'kr n, key': repeat key, 'ks keys': dispatch chars in keys


changeset:   17:68ba607001c2
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Sun Jun 17 02:26:43 2012 -0300
files:       remembercases/doers.py
description:
fix typo in doers.move_files


changeset:   16:1bbdd0f45a0b
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Sun Jun 17 01:54:51 2012 -0300
files:       remembercases/doers.py remembercases/test/test_doers.py
description:
added doers.pprint_to_text


changeset:   15:223a03dd4a4d
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Sat Jun 16 20:57:23 2012 -0300
files:       remembercases/doers.py
description:
added doers.move_files


changeset:   14:806ca72b2f39
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Sat Jun 16 02:02:33 2012 -0300
files:       remembercases/db.py remembercases/test/test_db.py
description:
added groups of props


changeset:   13:28dee3dda22a
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Mon Jun 11 10:37:41 2012 -0300
files:       remembercases/doers.py
description:
load_text now always returns text with LF EOLs, irrespective of EOLs style in the file


changeset:   12:492fa2f0895d
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Fri May 18 16:39:51 2012 -0300
files:       remembercases/db.py
description:
fixed db.history_add typo


changeset:   11:4c4db0d06d17
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Fri May 18 13:30:31 2012 -0300
files:       remembercases/db.py
description:
added db.update_entity_propdict


changeset:   10:3cc4716c2eae
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Fri May 18 12:59:02 2012 -0300
files:       remembercases/db.py
description:
added history, fixed msg


changeset:   9:3860b8da9fe4
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Tue May 15 18:34:51 2012 -0300
files:       remembercases/db.py remembercases/test/test_db.py
description:
upgraded db.entities to also filter a provided 'candidates' param. API backward incompatible due to changed return values


changeset:   8:e80a3bab054d
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Tue May 15 14:16:44 2012 -0300
files:       remembercases/doers.py
description:
added doers.md5_hex(text)


changeset:   7:275d80004ef3
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Tue May 15 14:15:35 2012 -0300
files:       remembercases/snapshot_taker.py
description:
fix crash due to string formatting mismatch in diagnostic


changeset:   6:3298ad1027e7
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Fri May 11 15:52:35 2012 -0300
files:       remembercases/test/test_db.py
description:
unit test db.num_entities


changeset:   5:6d69d2c2c638
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Fri May 11 15:42:21 2012 -0300
files:       remembercases/db.py
description:
added db.num_entities


changeset:   4:d8474db9797a
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Fri May 11 00:08:19 2012 -0300
files:       remembercases/test/test_db.py
description:
better test_db


changeset:   3:3a231c6672b3
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Thu May 10 23:38:22 2012 -0300
files:       remembercases/db.py remembercases/proxy.py remembercases/test/test_db.py
description:
better test_db, also eliminated a debug print


changeset:   2:3b030db6c33a
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Wed May 09 14:17:30 2012 -0300
files:       remembercases/cmd_timeout.py
description:
better cmd_timeout - needs docstring cleanup and assertive test


changeset:   1:453aa18c6112
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Tue May 08 18:15:28 2012 -0300
files:       README
description:
fixed reST sintax in README, repo URL added


changeset:   0:384a3731ed23
user:        Claudio Canepa <ccanepacc@gmail.com>
date:        Tue May 08 16:03:55 2012 -0300
files:       .hgignore CHANGELOG README remembercases/__init__.py remembercases/cmd_timeout.py remembercases/db.py remembercases/doers.py remembercases/proxy.py remembercases/samples/db.exercises.py remembercases/snapshot_taker.py remembercases/test/dummy_proxy.py remembercases/test/size_gen.py remembercases/test/test_ScreenSampler_parse_fullcommand.py remembercases/test/test_ScreenSampler_sampler.py remembercases/test/test_cmd_timeout.py remembercases/test/test_db.py remembercases/test/test_doers.py remembercases/test/test_proxy.py
description:
initial population


