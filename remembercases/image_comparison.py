# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals

"""
Initially the classic recipe for PIL image equality was used:
    equals(im1, im2) = ImageChops.difference(im1, im2).getbbox() is None

This broke (for RGBA images) with pillow >= 7.1.0, hence the numpy based comparison
"""
try:
    from PIL import Image
except ImportError:
    print('*** PIL and Pillow libraries not available, one must be installed. ***')

import numpy as np


def equal(fname1, fname2):
    """exact pixel to pixel match

    Probably good enough for lossless formats like png
    The files must be proper image files
    """
    im1 = Image.open(fname1)
    data1 = np.asarray(im1)
    im2 = Image.open(fname2)
    data2 = np.asarray(im2)
    return np.array_equal(data1, data2)


def equal_robust(fname1, fname2):
    """exact pixel to pixel match

    Probably good enough for lossless formats like png
    If any of the files can't be read as an image file, returns false.
    This includes file not present or corrupt files
    """
    try:
        im1 = Image.open(fname1)
        data1 = np.asarray(im1)
        im2 = Image.open(fname2)
        data2 = np.asarray(im2)
        res = np.array_equal(data1, data2)
    except Exception:
        res = False
    return res


def distance_01(fname1, fname2):
    if equal_robust(fname1, fname2):
        dist = 0.0
    else:
        dist = 1.0
    return dist


def is_full_black(fname):
    im = Image.open(fname)
    data = np.asarray(im)
    return not np.any(data)


def diff_bw(im1, im2):
    """Calculates a diff image by diff_pixel = black if pixel equals else white

    Output image is 'L', greyscale 8-bit pixels, uses only 0 (black) and 255 (white)

    Images expected to be same size and same mode (RGB, ...)
    Image modes supported: L, RGB, RGBA
    For RGBA it may make sense to first convert to RGB
    """
    out = None
    if im1.size != im2.size or im1.mode != im2.mode:
        diagnostic = "images different sizes or mode"
    else:
        out = Image.new('L', im1.size, 0)
        for x in range(im1.width):
            for y in range(im1.height):
                xy = (x, y)
                if im1.getpixel(xy) != im2.getpixel(xy):
                    out.putpixel((x, y), 255)
        diagnostic = ""
    return diagnostic, out


def diff_proportional(im1, im2):
    out = None
    if im1.size != im2.size or im1.mode != im2.mode:
        diagnostic = "images different sizes or mode"
    else:
        diagnostic = ""
        out = Image.new('L', im1.size, 0)
        if im1.mode == 'RGB':
            normalize_255 = 3
            for x in range(im1.width):
                for y in range(im1.height):
                    xy = (x, y)
                    if im1.getpixel(xy) != im2.getpixel(xy):
                        m = sum([abs(a - b) for a, b in zip(im1.getpixel(xy), im2.getpixel(xy))])
                        out.putpixel(xy, m // normalize_255)
        elif im1.mode == "RGBA":
            normalize_255 = 255 * 3
            for x in range(im1.width):
                for y in range(im1.height):
                    xy = (x, y)
                    if im1.getpixel(xy) != im2.getpixel(xy):
                        r, g, b, a = im1.getpixel(xy)
                        rr, gg, bb, aa = im2.getpixel(xy)
                        m = abs(r * a - rr * aa) + abs(g * a - gg * aa) + abs(b * a - bb * aa)
                        out.putpixel(xy, m // normalize_255)
        else:
            diagnostic = "images different sizes or mode"
    return diagnostic, out


# palette for diff_scale
dcolors = [
    (0, 0, 0),      # black
    (153, 0, 153),  # magenta
    (204, 0, 0),    # red
    (255, 102, 0),  # orange
    (255, 255, 0),  # yellow
]


def diff_scale(im1, im2):
    """Calculate a diff image by diff_pixel = f(abs(im1_pixel, im2_pixel))

    Output image is RGBA

    Colors has the approximate meaning
    black    equal
    magenta  delta < 10%
    red      delta < 20%
    orange   delta < 50%
    yellow   delta >= 50%

    Images expected to be same size and same mode (RGB, ...)
    Image modes supported: L, RGB, RGBA
    For RGBA it may make sense to first convert to RGB

    while a palettized image seems more natural, they are underdocumented in
    pillow, hence the output to RBG.
    """
    global dcolors
    out = None
    if im1.size != im2.size or im1.mode != im2.mode:
        diagnostic = "images different sizes or mode"
    else:
        out = Image.new('RGB', im1.size, dcolors[0])
        max_sum = 255 * len(im1.mode)
        for x in range(im1.width):
            for y in range(im1.height):
                xy = (x, y)
                if im1.getpixel(xy) != im2.getpixel(xy):
                    m = sum([abs(a - b) for a, b in zip(im1.getpixel(xy), im2.getpixel(xy))])
                    # reasonable for RGB, L, to a certain point RGBA
                    u = m / max_sum
                    if u < 0.1:
                        c = 1
                    elif u < 0.2:
                        c = 2
                    elif u < 0.5:
                        c = 3
                    else:
                        c = 4
                    out.putpixel(xy, dcolors[c])
        diagnostic = ""
    return diagnostic, out


def diff_binary(fname1, fname2, fout):
    im1 = Image.open(fname1)
    im2 = Image.open(fname2)
    if im1.size != im2.size or im1.mode != im2.mode:
        # error, different size or mode
        fmt = "images different sizes or mode: %s, %s"
        diagnostic = fmt % (fname1, fname2)
        return diagnostic
    out = Image.new('L', im1.size, 0)
    for x in range(im1.width):
        for y in range(im1.height):
            xy = (x, y)
            if im1.getpixel(xy) != im2.getpixel(xy):
                out.putpixel((x, y), 255)
    out.save(fout)
    diagnostic = ""
    return diagnostic


def cmp_and_diff(fname1, fname2, fout_diff_bw, fout_scale):
    equal = False
    comparables = True
    try:
        im1 = Image.open(fname1)
        data1 = np.asarray(im1)
        im2 = Image.open(fname2)
        data2 = np.asarray(im2)
        equal = np.array_equal(data1, data2)
    except Exception as ex:
        print("Exception:", ex)
        pass

    print("cmp_and_diff - equals:", equal)

    if equal:
        return equal, comparables

    comparables = im1.size == im2.size and im1.mode == im2.mode
    if not comparables:
        return equal, comparables

    # do diff bw and diff scaled in parallel
    bw = Image.new('L', im1.size, 0)
    scaled = Image.new('RGB', im1.size, dcolors[0])
    max_sum = 255 * len(im1.mode)
    for x in range(im1.width):
        for y in range(im1.height):
            xy = (x, y)
            if im1.getpixel(xy) != im2.getpixel(xy):
                bw.putpixel((x, y), 255)
                m = sum([abs(a - b) for a, b in zip(im1.getpixel(xy), im2.getpixel(xy))])
                # reasonable for RGB, L, to a certain point RGBA
                u = m / max_sum
                if u < 0.1:
                    c = 1
                elif u < 0.2:
                    c = 2
                elif u < 0.5:
                    c = 3
                else:
                    c = 4
                scaled.putpixel((x, y), dcolors[c])

    bw.save(fout_diff_bw)
    scaled.save(fout_scale)
    return equal, comparables


def cmp_and_diff_proportional(fname1, fname2, fout_diff_bw, fout_proportional):
    """
    im1, im2 should have same size and mode, mode should be one of "RGBA", "RGB"
    """
    equal = False
    comparables = True
    try:
        im1 = Image.open(fname1)
        data1 = np.asarray(im1)
        im2 = Image.open(fname2)
        data2 = np.asarray(im2)
        equal = np.array_equal(data1, data2)
    except Exception as ex:
        print("Exception:", ex)
        pass

    print("cmp_and_diff - equals:", equal)

    if equal:
        return equal, comparables

    comparables = (im1.size == im2.size
                   and im1.mode == im2.mode
                   and im1.mode in ["RGBA", "RGB"])

    if not comparables:
        return equal, comparables

    # do diff bw and diff proportional in parallel
    bw = Image.new('L', im1.size, 0)
    prop = Image.new('L', im1.size, 0)

    if im1.mode == "RGBA":
        normalize_255 = 255 * 3
        for x in range(im1.width):
            for y in range(im1.height):
                xy = (x, y)
                if im1.getpixel(xy) != im2.getpixel(xy):
                    bw.putpixel((x, y), 255)

                    r, g, b, a = im1.getpixel(xy)
                    rr, gg, bb, aa = im2.getpixel(xy)
                    m = abs(r * a - rr * aa) + abs(g * a - gg * aa) + abs(b * a - bb * aa)
                    prop.putpixel(xy, m // normalize_255)

    if im1.mode == "RGB":
        normalize_255 = 255 * 3
        for x in range(im1.width):
            for y in range(im1.height):
                xy = (x, y)
                if im1.getpixel(xy) != im2.getpixel(xy):
                    bw.putpixel((x, y), 255)

                    m = sum([abs(a - b) for a, b in zip(im1.getpixel(xy), im2.getpixel(xy))])
                    prop.putpixel(xy, m // normalize_255)

    bw.save(fout_diff_bw)
    prop.save(fout_proportional)
    return equal, comparables
