from __future__ import division, print_function, unicode_literals

try:
    from PIL import Image, ImageChops
except ImportError:
    print('*** PIL and Pillow libraries not available, one must be installed. ***')


def equal_PIL(fname1, fname2):
    """exact pixel to pixel match

    Probably good enough for loseless formats like png
    The files must be proper image files
    """
    im1 = Image.open(fname1)
    im2 = Image.open(fname2)
    return ImageChops.difference(im1, im2).getbbox() is None


def equal_PIL_robust(fname1, fname2):
    """exact pixel to pixel match

    Probably good enough for loseless formats like png
    If any of the files can't be readed as an image file, returns false.
    This includes file not present or corrupt files
    """
    try:
        im1 = Image.open(fname1)
        im2 = Image.open(fname2)
        res = ImageChops.difference(im1, im2).getbbox() is None
    except Exception:
        res = False
    return res


def distance_01(fname1, fname2):
    if equal_PIL_robust(fname1, fname2):
        dist = 0.0
    else:
        dist = 1.0
    return dist

def is_full_black(fname):
    im = Image.open(fname)
    return not im.getbbox()
