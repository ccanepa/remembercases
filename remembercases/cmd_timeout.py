# -*- coding: utf-8 -*-
"""
Support to run a python script in a subprocess, killing the subprocess if the
run time exceeds a timeout. Must run in Windows and *nix.

stdout, stderr from the subprocess are captured, returncode is reported,
also timeout status is reported.

Current implementations can miss some of stdout, stderr when timeout is hit,
exact details vary with platform (see details in function docstrings).

Support in python itself for this is coming in python 3.3, see
http://bugs.python.org/issue5673

Code inspired by
http://stackoverflow.com/questions/3575554/python-subprocess-with-timeout-and-large-output-64k
http://betabug.ch/blogs/ch-athens/1093
"""
from __future__ import division, print_function, unicode_literals
import six

import subprocess
import threading
import sys
import time


def _decode_communicate_py2(bs):
    try:
        s = bs.decode("ascii")
    except UnicodeDecodeError:
        s = "repr: " + unicode(repr(bs))
    return s


def _decode_communicate_py3(bs):
    try:
        s = bs.decode("latin1")
    except UnicodeDecodeError:
        s = "repr: " + str(repr(bs))
    return s


if six.PY2:
    decode_communicate = _decode_communicate_py2
else:
    decode_communicate = _decode_communicate_py3

def cmd__py3(cmdline, bufsize=-1, cwd=None, timeout=60):
    # use the same python as the caller to run the script
    cmdline.insert(0, "-u")
    cmdline.insert(0, sys.executable)

    p = subprocess.Popen(
        cmdline,
        bufsize = bufsize,
        shell   = False,
        stdout  = subprocess.PIPE,
        stderr  = subprocess.PIPE,
        cwd     = cwd
    )
    killed = True
    try:
        out, err = p.communicate(timeout=timeout)
        killed = False
    except subprocess.TimeoutExpired:
        p.kill()
        out, err = p.communicate()

    out = decode_communicate(out)
    err = decode_communicate(err)
    returncode = p.returncode

    return killed, returncode, err, out
    

# better, no problem with output size
def cmd__uses_event(cmdline, bufsize=-1, cwd=None, timeout=60):
    """
    Runs a python script in a subprocess, killing the subprocess if the
    run time exceeds a timeout.
    The python executable running the script will be the same that runs
    the calling code.

    Parameters:
        cmdline: list
            as in the list style of subprocess.Popen, not the string style.
            the first element is the python script name, followed by
            args to be passed to the script
        cwd: string or None
            If cwd is not None, the child’s current directory will be
            changed to cwd before it is executed.
        bufsize: int
            as in subprocess.Popen
        timeout: float
            time in seconds, if the subprocess takes more than timeout
            seconds to complete, it is killed

    Returns a tuple (killed, returncode, err, out) where
        killed: bool
            True if subprocess killed after exceeding timeout
        returncode: int
            as in subprocess, except is 1 in windows if timeout hit
        err: unicode in py2, str in py3
            stderr from the subprocess (but see warnings below)
        out: unicode in py2, str in py3
            stdout from the subprocess (but see warnings below)

    WARNINGS:
       Due to rules for print implicit encoding and how subprocess
       changes sys.stdout and sys.stderr, a script that runs without
       tracebacks when exercised directly from the command line can fail with
       an UnicodeDecodeError exception when running trough this function, namely
       when:
           in py2.6,  writing outside of the ascii charset
           in py3.3,  writing outside of the latin1 charset

       As subprocess communicate returns bytes, if the conversion to text fails
       with a decode error, then 'out' will carry "repr: " + unicode(repr(stdout))
       Similar with stderr. The bytes encoding is assumed to be 'ascii' in py2.6
       and 'latin1' in py3.
       
    Needs at least python 2.6 (for subprocess.Popen.kill)

    Uses subprocess and threading.

    NOTICE: the following was before adding '-u' to the spawned python (thanks
    Daniel Moisset) - needs to cleanup this docstring. Now they are no missing
    bytes in any case

    In python 2.6.6, winXP, (bufsize has no influence in the following)
        timeout is honored

        when standalone script finishes before timeout, output is captured at
        least for len < 1MB

        when standalone script hits the timeout, output is captured except for
        the last len % 4096 bytes (tested for len < 1MB).
        Notice that in this case, if len < 4K, we see nothing of the output.

    In windows returncode is the one of subprocess if timeout was not hit;
    1 if hit
    """
    # use the same python as the caller to run the script
    cmdline.insert(0, "-u")
    cmdline.insert(0, sys.executable)

    kill_check = threading.Event()

    def _kill_after_timeout(subproc):
        subproc.kill()
        kill_check.set()  # tell the main routine that we had to kill

    p = subprocess.Popen(
        cmdline,
        bufsize = bufsize,
        shell   = False,
        stdout  = subprocess.PIPE,
        stderr  = subprocess.PIPE,
        cwd     = cwd
    )
    watchdog = threading.Timer(timeout, _kill_after_timeout, args=(p, ))
    watchdog.start()

    out, err = p.communicate()

    watchdog.cancel() # if it's still waiting to run
    killed = kill_check.isSet()
    kill_check.clear()

    out = decode_communicate(out)
    err = decode_communicate(err)
    returncode = p.returncode

    return killed, returncode, err, out


# less desirable if output exceeds 4KB
def cmd__uses_poll(cmdline, bufsize=-1, cwd=None, timeout=60):
    """
    Runs a python script in a subprocess, killing the subprocess if the
    run time exceeds a timeout.
    The python executable running the script will be the same that runs
    the calling code.

    Parameters:
        cmdline: list
            as in the list style of subprocess.Popen, not the string style.
            the first element is the python script name, followed by
            args to be passed to the script
        cwd: string or None
            If cwd is not None, the child’s current directory will be
            changed to cwd before it is executed.
        bufsize: int
            as in subprocess.Popen
        timeout: float
            time in seconds, if the subprocess takes more than timeout
            seconds to complete, it is killed

    Returns a tuple (killed, returncode, err, out) where
        killed: bool
            True if subprocess killed after exceeding timeout
        returncode: int
            as in subprocess, except is 1 in windows if timeout hit
        err: str
            stderr from the subprocess (can be incomplete, see below)
        out: str
            stdout from the subprocess (can be incomplete, see below)
    Needs at least python 2.6 (for subprocess.Popen.kill)

    Uses subprocess only.

    In python 2.6.6, winXP, (bufsize has no influence in the following)
        timeout is honored.

        When standalone script finishes before timeout
            all output is captured for len <= 4K
            first 4K of output is captured for 4K < len < 8K, and timeout is hit
            captured output is '' for len >= 8K, and timeout is hit
        In the last two cases the elapsed time is an artifact of the stdout,
        stderr blocking read.
                    
        When standalone script finishes after timeout
            For 4K<= len <8K the first 4K of output is captured, timeout is hit
            For all other values of len captured output is ''; timeout is hit

    In windows returncode is the one of subprocess if timeout was not hit;
    1 if hit
    """
    # use the same python as the caller to run the script
    cmdline.insert(0, "-u")
    cmdline.insert(0, sys.executable)
  
    p = subprocess.Popen(
        cmdline,
        bufsize = bufsize,
        shell   = False,
        stdout  = subprocess.PIPE,
        stderr  = subprocess.PIPE,
        cwd     = cwd
    )

    t_begin = time.time()
    seconds_passed = 0

    cnt = 0
    while p.poll() is None and seconds_passed < timeout:
        seconds_passed = time.time() - t_begin
        time.sleep(0.1)
        cnt += 1
#    print('cnt, seconds_passed:', cnt, seconds_passed)

    killed = seconds_passed > timeout
    if killed:
        p.kill()

    out, err = p.communicate()
    out = decode_communicate(out)
    err = decode_communicate(err)

    returncode = p.returncode

    return killed, returncode, err, out

if six.PY2:
    run_script = cmd__uses_event
else:
    run_script = cmd__py3


## note to me:
##    Some example code called subprocess.Popen with
##        cmdline = ['exp_gen.py', size]
##        shell = False
##    With this, in win xp, python 2.6.6 I got a traceback in subprocess.py:
##        WindowsError: [Error 193] %1 not a valid Win32 app
##
##    Two ways to get rid off that were
##        cmdline = [sys.executable, 'exp_gen.py', size]
##        shell = False
##    or
##        cmdline = ['exp_gen.py', size]
##        shell = True
##    The later is not exactly equivalent, if I understood well the p.pid here
##    would be the one of the shell, not the one of spawned python process.

