# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals
import six

import os
import hashlib
import shutil
import pprint

# >>> building the logical set of target files  >>>>>>>>>>>>>>>>>>>>


def files_from_dir(initial_dir, fn_allow_filename=None, recurse=False):
    initial_dir = os.path.normpath(initial_dir)
    if fn_allow_filename is None:
        fn_allow_filename = lambda x: True
    if not os.path.isdir(initial_dir):
        raise ValueError("Initial dir not found or not a dir: %s" % initial_dir)
    all_files = []
    for dirpath, subdirs, files in os.walk(initial_dir):
        all_files.extend([os.path.join(dirpath, f)
                          for f in files if fn_allow_filename(f)])
        if not recurse:
            del subdirs[:]
    return all_files


def clean_line(s, end_mark=None):
    """
    Characters between the first appearance of end_mark and the end of string
    are discarded; then trailing whitespace is discarded
    """
    """First it discards any character after the first end_mark character,
       (including the mark)
       Then it discard any leading and trailing whitespace
    """
    if end_mark is not None:
        i = s.find(end_mark)
        if i > -1:
            s = s[:i]
    return s.strip()


def scripts_names_from_text(text, end_mark=None):
    """" text -> set of script names parsed from the text

    A script name must fit in one line
    At most only one script name per line
    For each line, characters after 'end_mark' are discarded (inclusive),
    then leading and trailing spaces are discarded. If this results in an
    empty line, the line is discarded.
    """

    all_lines = [clean_line(line, end_mark) for line in text.split('\n')]
    script_names = set([s for s in all_lines if len(s) > 0])
    return script_names


# >>> task helpers  >>>>>>>>>>>>>>>>>>>>

def load_text(filename):
    """
    Returns the content of file as (unicode) text; with eol normalized to '\n'
    May raise UnicodeDecodeError if the file is not encoded in utf-8

    """
    if six.PY2:
        text = _load_text_py2(filename)
    else:
        text = _load_text_py3(filename)
    return text.replace("\r\n", "\n")


def _load_text_py2(filename):
    with open(filename, 'rb') as f:
        as_bytes = f.read()
    text = as_bytes.decode("utf-8")
    return text


def _load_text_py3(filename):
    # no eol conversion (newline='') for parity with the py2 version
    with open(filename, "r", encoding="utf-8", newline='') as f:
        text = f.read()
    return text


class HasherMD5(object):
    """helper class to hash a sequence of (unicode) text fragments

       example::
           h = HasherMD5()
           for text in text_fragments:
               h.update(text)
           hash = h.hash_as_text()
    """
    def __init__(self):
        self.h = hashlib.md5()

    def update(self, text):
        self.h.update(text.encode("utf-8"))

    def hash_as_text(self):
        return six.text_type(self.h.hexdigest())


def md5_hex_for_text(text):
    h = HasherMD5()
    h.update(text)
    return h.hash_as_text()


def md5_hex(data_as_bytes):
    return hashlib.md5(data_as_bytes).hexdigest()


def move_files(candidates, src_dir, dst_dir):
    """
    moves the files in candidates from src_dir to dst_dir, overwriting if
    necessary.

    Returns (move, cant_move)
        move: set with candidates successfully moved
        cant_move: set with candidates that cant be moved (non existing,
        permissions, etc...)
    """
    cant_move = set()
    for name in candidates:
        src = os.path.join(src_dir, name)
        dst = os.path.join(dst_dir, name)
        try:
            shutil.move(src, dst)
        except Exception:
            cant_move.add(name)

    moved = set([k for k in candidates if k not in cant_move])
    return moved, cant_move


def pprint_to_string(obj):
    class AStream(object):
        def __init__(self):
            self.parts = []

        def write(self, s):
            self.parts.append(s)

        def __str__(self):
            return ''.join(self.parts)

    a = AStream()
    pprint.pprint(obj, a)
    return a.__str__()


def join_sorted(strings):
    parts = [e for e in strings]
    parts.sort()
    text = '\n'.join(parts)
    return text
