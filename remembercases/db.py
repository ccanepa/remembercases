# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals

import os
import pickle
import copy

LOG_SEPARATOR = '\n*>'


def db_save(db, filename):
    with open(filename, 'wb') as f:
        db.save(f)


def db_load(filename, default_testbed=None):
    with open(filename, 'rb') as f:
        new = TestbedEntityPropDB.new_from_file(f)
    if default_testbed is not None:
        new.set_default_testbed(default_testbed)
    return new


def get_entities(db, fn_allow=None, candidates=None, testbed=None):
    """returns a subset of 'candidates', unknowns if fn_allow is None, else
       the ones in candidates that are in the db testbed and make fn_allow True

       db: an TestbedEntityPropDB instance

       fn_allow: (f: propdict -> bool) or None
         A function that receives an entity's propdict, and returns
            True : if entity will be selected
            False: of entity will not be selected
         None signals  'return the ones in candidates not present in db testbed'

       candidates: iterable of entities to filter
         None means all entities in db testbed

       testbed: a tesbed known to the db
         None means the current db's default testbed

    candidates entities that aren't in the db testbed are silently ignored

    This is a convenience variation on db.entities method, mainly used for
    reporting.

    Notice that fn_allow==None has opposite meaning in get_entities than in
    method db.entities.

    Except when calling with 'candidates' = None, the use of db.entities method
    is preferred, the silent ignoring in get_entities can be dangerous.
    """
    want_unknowns = (fn_allow is None)
    if want_unknowns:
        fn_allow = lambda x: True
    selected, unknown = db.entities(fn_allow=fn_allow,
                                    candidates=candidates,
                                    testbed=testbed)

    if want_unknowns:
        selected = unknown

    return selected


class ErrorUnknownTestbed(KeyError):
    pass


class ErrorUnknownEntity(KeyError):
    pass


class ErrorUnknownProp(KeyError):
    pass


class ErrorUnknownPropInEntity(KeyError):
    pass


class ErrorUnknownGroup(KeyError):
    pass


class ErrorUnknownPropInGroup(KeyError):
    pass


class ErrorDuplicatedTestbed(ValueError):
    pass


class ErrorDuplicatedGroup(ValueError):
    pass


class ErrorDuplicatedProp(ValueError):
    pass


class TestbedEntityPropDB(object):
    @classmethod
    def new_from_file(cls, infile):
        """
        creates a new cls object and sets the testbeds related info from infile

        infile data as produced in a .save call

        Info not associated with a particular testbed will have the same value
        as in a new empty instance, by example current testbed will be None

        If the infile object comes from a file, it must have been opened as 'rb'
        """
        unpickler = pickle.Unpickler(infile)
        new = cls()
        new.db = unpickler.load()
        return new

    def __init__(self):
        self.db = {}  # testbed: <testbed_data> pairs
        self.default_testbed = None

    def save(self, outfile):
        """
        stores all info related to tesbeds in outfile

        Info not associated with a particular testbed is not stored,
        by example default testbed is not stored

        If the outfile object comes from a file, it must have been opened as 'wb'
        """
        # protocol 2 is compatible with both py2.x and 3.x , protocol 3 is py3 only
        pickler = pickle.Pickler(outfile, protocol=2)
        pickler.dump(self.db)

    def __iter__(self):
        """iterator over all testbeds known to the db"""
        return iter(self.db)

    def add_testbed(self, testbed, basepath):
        """
        testbed must be hashable

        testbed can't take the value None

        No duplicated testbeds are allowed; ErrorDuplicatedTestbed will be
        raised when attempting to add a testbed which was previously added

        basepath would be used as the root for al paths stored in the testbed
        and should be considered Read Only for the entire life of testbed
        No test over its value is performed.
        """
        if testbed is None:
            raise ValueError("testbed can't be None")
        if testbed in self.db:
            raise ErrorDuplicatedTestbed("Duplicated testbed:%s" % testbed)

        testbed_data = {}

        # initialize testbed data
        testbed_data['known_props'] = set()
        testbed_data['groups'] = {}  # name: set(propnames)
        testbed_data['entities'] = {}  # name: propdict ; propdict as propname: value
        testbed_data['basepath'] = basepath
        testbed_data['notes'] = None
        testbed_data['log'] = ''
        testbed_data['history'] = []
        self.db[testbed] = testbed_data

    def set_default_testbed(self, testbed):
        """
        Sets the testbed to be used when a method signature includes
        'testbed=None' and a call to that method don't specifies a testbed

        returns the testbed that was the default before the call, can be None

        Raises ErrorUnknownTestbed if testbed is not a known testbed or None
        """
        if testbed not in self.db:
            raise ErrorUnknownTestbed("Unknown testbed:%s" % testbed)
        old_testbed = self.default_testbed
        self.default_testbed = testbed
        return old_testbed

    def good_testbed(self, testbed):
        """
        If testbed is None , returns the default testbed.
        For other values it verifies that testbed refers to an existing testbed,
        and returns the value unchanged.
        raises ErrorUnknownTestbed if unknown testbed.

        Should be called at the start of any method taking a testbed parameter.
        """
        if testbed is None:
            if self.default_testbed is None:
                raise KeyError('Wanted default testbed but default was not set')
            testbed = self.default_testbed
        elif testbed not in self.db:
            raise ErrorUnknownTestbed('Unknown testbed: %s' % testbed)
        return testbed

    def clone_testbed(self, src, dst, candidates=None):
        """
        src == None is interpreted as default testbed
        only entities in candidates are cloned to the new testbed
        candidates not in src are silently ignored
        """
        src = self.good_testbed(src)
        if src == dst:
            msg = "src and dst can't be the same testbed; testbed:%s" % src
            raise ErrorDuplicatedTestbed(msg)
        basepath = self.db[src]['basepath']
        self.add_testbed(dst, basepath)

        src_ents = self.db[src]['entities']
        dst_ents = self.db[dst]['entities']
        for e in self.db[src]:
            if e == 'entities':
                # only copy candidates; also ignore the ones not known in src
                known, unknown = self.entities(None, candidates, src)
                for entity in known:
                    dst_ents[entity] = copy.deepcopy(src_ents[entity])
            else:
                self.db[dst][e] = copy.deepcopy(self.db[src][e])

    def rename_testbed(self, old, new):
        """renames a testbed, the db.default_testbed is updated if necessary

        Neither new or old can be None

        returns the resulting db.default_testbed
        """
        if new is None or old is None:
            raise ValueError("testbed can't be None")

        if old not in self.db:
            raise ErrorUnknownTestbed("Unknown testbed:%s" % old)

        if new == old:
            return self.default_testbed

        if new in self.db:
            raise ErrorDuplicatedTestbed("Duplicated testbed:%s" % new)

        self.db[new] = self.db[old]
        del self.db[old]

        if old == self.default_testbed:
            self.default_testbed = new
        return self.default_testbed

    def del_testbed(self, testbed=None):
        testbed = self.good_testbed(testbed)
        if testbed == self.default_testbed:
            self.default_testbed = None
        del self.db[testbed]

    def add_prop(self, propname, testbed=None):
        """
        Makes propname available as a property name for entities in testbed.

        propname must be hashable, and duplicates of props known in the testbed
        are not allowed.
        testbed must be a known testbed or None to use the default testbed.
        """
        testbed = self.good_testbed(testbed)
        if propname in self.db[testbed]["known_props"]:
            raise ErrorDuplicatedProp("Prop was added before: %s" % propname)
        self.db[testbed]["known_props"].add(propname)

    def good_prop(self, prop, testbed):
        """
        Verifies that the prop is known in the testbed

        Returns None; raises ErrorUnknownProp if prop is unknown in testbed
        This method should be called near the start of methods that take one or
        more props as parameters.
        It is expected that testbed is the result of a good_testbed call.
        """
        if prop not in self.db[testbed]['known_props']:
            msg = "Unknown prop '%s' in testbed '%s'" % (prop, testbed)
            raise ErrorUnknownProp(msg)

    def add_group(self, groupname, propnames, allow_redefine=False,
                  addprops=False, testbed=None):
        """
        Groups a number of props under a name
        """
        testbed = self.good_testbed(testbed)
        all_groups = self.db[testbed]['groups']
        if not allow_redefine and groupname in all_groups:
            msg = "Attempt to redefine group '%s' in testbed '%s'"
            msg = msg % (groupname, testbed)
            raise ErrorDuplicatedGroup(msg)
        known_props = self.db[testbed]['known_props']
        candidates = set([k for k in propnames])
        if addprops:
            duplicated = [k for k in candidates if k in known_props]
            if duplicated:
                msg = "Prop(s) added before, in group '%s', testbed '%s':%s"
                msg = msg % (groupname, testbed, candidates)
                raise ErrorDuplicatedProp(msg)
            else:
                for name in candidates:
                    self.add_prop(name, testbed)
        else:
            unknowns = [k for k in candidates if k not in known_props]
            if unknowns:
                msg = "Unknown props in group '%s', testbed '%s':%s"
                msg = msg % (groupname, testbed, unknowns)
                raise ErrorUnknownProp(msg)
        all_groups[groupname] = candidates

    def good_group(self, groupname, testbed):
        """
        Verifies that groupname is a known group in the testbed

        Returns None; raises ErrorUnknownGroup if groupname unknown in testbed
        """
        if groupname not in self.db[testbed]['groups']:
            msg = "Unknown group '%s' in testbed '%s'" % (groupname, testbed)
            raise ErrorUnknownGroup(msg)

    def add_entity(self, entity, testbed=None):
        testbed = self.good_testbed(testbed)
        if entity in self.db[testbed]['entities']:
            msg = "Duplicate Entity %s in testbed %s" % (entity, testbed)
            raise ValueError(msg)
        self.db[testbed]['entities'][entity] = {}

    def del_entity(self, entity, testbed=None):
        """if entity present, eliminates entity from the db"""
        testbed = self.good_testbed(testbed)
        if entity in self.db[testbed]['entities']:
            del self.db[testbed]['entities'][entity]

    def set_prop_value(self, entity, prop, value, testbed=None):
        testbed = self.good_testbed(testbed)
        self.good_prop(prop, testbed)
        entities = self.db[testbed]['entities']
        if entity not in entities:
            msg = "Unknown entity %s in testbed %s" % (entity, testbed)
            raise ErrorUnknownEntity(msg)
        self.db[testbed]['entities'][entity][prop] = copy.deepcopy(value)

    def get_prop_value(self, entity, prop, testbed=None):
        testbed = self.good_testbed(testbed)
        self.good_prop(prop, testbed)
        if entity not in self.db[testbed]['entities']:
            msg = "Unknown entity %s in testbed %s" % (entity, testbed)
            raise ErrorUnknownEntity(msg)
        propdict = self.db[testbed]['entities'][entity]
        if prop not in propdict:
            msg = "Unknown prop '%s' in entity '%s' in testbed '%s'"
            msg = msg % (prop, entity, testbed)
            raise ErrorUnknownPropInEntity(msg)
        return copy.deepcopy(propdict[prop])

    def entities(self, fn_allow=None, candidates=None, testbed=None):
        """
        returns (allowed_entities, unknown_entities) where

        allowed_entities:
            collection of entities in candidates known in the testbed 'testbed'
            which also makes fn_allow(entity propdict) True.
        unknown_entities:
            collection of entities in candidates unknown in the testbed 'testbed'

        fn_allow:
            filter function or None
            filter function is a fn_allow(entity propdict) -> T/F .
            If fn_allow==None, it is assumed fn_allow allows all

        candidates:
            iterable normally yielding know entities. If None, all known
            entities in the db are assumed.

        If you simply want to know which objects in 'candidates' are unknown in
        the testbed disregarding any property, choose
            fn_allow = lambda x: False
        and take the 'unknown' part in the method's return.
        """
        testbed = self.good_testbed(testbed)
        if fn_allow is None:
            fn_allow = lambda x: True
        all_entities = self.db[testbed]['entities']
        if candidates is None:
            entities = all_entities
            unknown_entities = set()
        else:
            unknown_entities = set([e for e in candidates if e not in all_entities])
            entities = [e for e in candidates if e in all_entities]

        allowed_entities = set([e for e in entities if fn_allow(all_entities[e])])
        return allowed_entities, unknown_entities

    def set_propdict(self, entity, propdict, testbed=None):
        """
        props for entity are deleted and then k:w in propdict are used to make
        new props for entity.
        """
        testbed = self.good_testbed(testbed)

        known_props = self.db[testbed]['known_props']
        unknown_props = [k for k in propdict if k not in known_props]
        if unknown_props:
            unknown_props.sort()
            msg = "propdict has prop(s) unknown in testbed %s; offending props: %s"
            msg = msg % (testbed, unknown_props)
            raise ErrorUnknownProp(msg)

        if entity not in self.db[testbed]['entities']:
            msg = "Unknown entity '%s' in testbed '%s'" % (entity, testbed)
            raise ErrorUnknownEntity(msg)
        self.db[testbed]['entities'][entity] = copy.deepcopy(propdict)

    def get_propdict(self, entity, testbed=None):
        """
        Returns a dict with all the props the entity have, prop: value pairs.
        Modifying the returned dict don't modifies the entity.
        """
        testbed = self.good_testbed(testbed)
        if entity not in self.db[testbed]['entities']:
            msg = "Unknown entity '%s' in testbed '%s'" % (entity, testbed)
            raise ErrorUnknownEntity(msg)
        return copy.deepcopy(self.db[testbed]['entities'][entity])

    def del_entity_props(self, entity, iterprops, testbed=None):
        """
        props in iterprops are deleted in the target entity, props not in the
        entity propdict are silently ignored
        """
        testbed = self.good_testbed(testbed)
        if entity not in self.db[testbed]['entities']:
            msg = "Unknown entity '%s' in testbed '%s'" % (entity, testbed)
            raise ErrorUnknownEntity(msg)
        propdict = self.db[testbed]['entities'][entity]
        for e in iterprops:
            if e in propdict:
                del propdict[e]

    def update_entity_propdict(self, entity, propdict, testbed=None):
        """
        Key value pairs in propdict are verified to comply that key is a known
        testbed's prop, then the entity's prop key is set to value.
        Key is not required to have been previously present in entity.
        If any key is invalid, no changes are done in the db.
        """
        testbed = self.good_testbed(testbed)
        if entity not in self.db[testbed]['entities']:
            msg = "Unknown entity '%s' in testbed '%s'" % (entity, testbed)
            raise ErrorUnknownEntity(msg)

        known_props = self.db[testbed]['known_props']
        unknown_props = [k for k in propdict if k not in known_props]
        if unknown_props:
            unknown_props.sort()
            msg = "propdict has prop(s) unknown in testbed %s; offending props: %s"
            msg = msg % (testbed, unknown_props)
            raise ErrorUnknownProp(msg)

        propdict = copy.deepcopy(propdict)
        self.db[testbed]['entities'][entity].update(propdict)

    def get_groupdict(self, entity, groupname, testbed=None):
        """
        Returns a dict of entity's props, only props both in group and entity
        """
        testbed = self.good_testbed(testbed)
        if entity not in self.db[testbed]['entities']:
            msg = "Unknown entity '%s' in testbed '%s'" % (entity, testbed)
            raise ErrorUnknownEntity(msg)
        propdict = self.db[testbed]['entities'][entity]
        self.good_group(groupname, testbed)
        keys = self.db[testbed]['groups'][groupname]
        return dict([(k, copy.deepcopy(propdict[k])) for k in keys if k in propdict])

    def del_groupdict(self, entity, groupname, testbed=None):
        """
        Deletes all props in entity whose name is in the group groupname
        """
        testbed = self.good_testbed(testbed)
        if entity not in self.db[testbed]['entities']:
            msg = "Unknown entity '%s' in testbed '%s'" % (entity, testbed)
            raise ErrorUnknownEntity(msg)
        propdict = self.db[testbed]['entities'][entity]
        self.good_group(groupname, testbed)
        groupdict = self.db[testbed]['groups'][groupname]
        keys = [k for k in groupdict if k in propdict]
        for k in keys:
            del propdict[k]

    def set_groupdict(self, entity, groupname, groupdict, testbed=None):
        """
        All props that belongs to both entity and group are removed from entity,
        then prop:values pairs in groupdict will be added to entity.

        Props in groupdict must be props in the designated group.
        """
        testbed = self.good_testbed(testbed)
        self.good_group(groupname, testbed)
        group_keys = self.db[testbed]['groups'][groupname]
        bad_keys = [k for k in groupdict if k not in group_keys]
        if bad_keys:
            msg = "Unknown prop in group '%s', testbed '%s':%s"
            msg = msg % (groupname, testbed, bad_keys)
            raise ErrorUnknownPropInGroup(msg)
        self.del_groupdict(entity, groupname, testbed)
        propdict = copy.deepcopy(groupdict)
        self.db[testbed]['entities'][entity].update(propdict)

    def canonical_fname(self, path, testbed=None):
        """
        given a path returns the canonical form suitable to store in db

        paths to be stored in the db should be expressed in a canonical form:
            1. relative to testbed's basepath
            2. with unix slashes ('/')
        """
        testbed = self.good_testbed(testbed)
        name = os.path.relpath(path, self.db[testbed]['basepath'])
        if os.sep != '/':
            name = name.replace(os.sep, '/')
        return name

    def fname_from_canonical(self, canonical_name, testbed=None):
        """
        given a canonical name for a file it returns a real path
        """
        testbed = self.good_testbed(testbed)
        if os.sep != '/':
            canonical_name = canonical_name.replace('/', os.sep)
        path = os.path.join(self.db[testbed]['basepath'], canonical_name)
        return path

    def set_notes(self, content, testbed=None):
        """
        sets the testbed's notes to the opaque object content

        content is opaque to the db
        No checks or changes are performed over content
        Typical use is to store text.
        For text, it is better to use an unicode object.
        """
        testbed = self.good_testbed(testbed)
        self.db[testbed]['notes'] = content

    def get_notes(self, testbed=None):
        """
        returns the last object stored in tesbed notes, or None if wasn't set

        content is opaque to the db
        No checks or changes are performed over content
        Typical use is to store text.
        For text, it is better to use an unicode object.
        """
        testbed = self.good_testbed(testbed)
        return self.db[testbed]['notes']

    def log(self, text, testbed=None):
        """
        Appends text to the testbed's log

        Meant to document progress in a high level task.
        Successive log entries are separated by LOG_SEPARATOR string
        User functions doing a domain task should record the action performed.

        Examples log messages:
            Enforced 'no tabs in test scripts'
            Simplest test identified
            Added testinfo to simplest scripts
            Updated prop 'has testinfo'
            ...
        """
        testbed = self.good_testbed(testbed)
        self.db[testbed]['log'] += LOG_SEPARATOR + text

    def export_log(self, testbed=None):
        """
        returns the testbed log contents
        """
        testbed = self.good_testbed(testbed)
        return self.db[testbed]['log']

    def num_entities(self, testbed=None):
        testbed = self.good_testbed(testbed)
        return len(self.db[testbed]['entities'])

    def history_add(self, operation, info, testbed=None):
        """
        Adds as last element of testbed's history an element containing

        operation and info
        operation and info should be text.
        Character '\n' should not be present into operation string
        """
        testbed = self.good_testbed(testbed)
        self.db[testbed]['history'].append('\n'.join([operation, info]))

    def history_find(self, range_=None, options=None, string_in_info=None):
        """
        range_ a pair with the slice semantics (-n -> len()-n; None -> 1st or last
        options : select items with option in options (None -> all)
        string_in_info / regex_in_info: search in text for the item found a match,
        None means no match required

        returns ?
        """
        raise NotImplementedError
