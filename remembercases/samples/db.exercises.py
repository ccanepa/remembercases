# -*- coding: utf-8 -*-
# when doing tdd some focus is needed. I made some exercises / use cases that
# helps with 'what next?'
# you see here a static view of the code, but in real life each exercise begins
# with some broad intention enunciation in the function's docstring, and the
# code grows toward that intention.
# You add some lines, run, see if it fail due to non implemented code, write
# unit test, write code. Repeat until the intention is fully implemented.
# Use example may need corrections as the library code evolves.

import remembercases.db as dbs


def initial_exercise():
    """
    Create a db
    add a testbed
    add a prop
    add some trivial entities
    set the entity's prop
    report
    """
    db = dbs.TestbedEntityPropDB()
    testbed = 'g945'
    db.add_testbed(testbed, '../..')
    db.set_default_testbed(testbed)

    db.add_prop('present')

    entities = ['ent0', 'ent1', 'ent2']
    prop_values = [True, False, True]
    for name, propvalue in zip(entities, prop_values):
        db.add_entity(name)
        db.set_prop_value(name, 'present', propvalue)

    print('testing some queries')
    print("entity 'ent0' property 'present' value:", db.get_prop_value('ent0', 'present'))

    print("Entities with prop 'present' == True")

    def fn_present_true(propdict):
        return 'present' in propdict and propdict['present']

    for name in db.entities(fn_present_true):
        print('\t', name)

    print('\nall entities:')
    for name in db.entities(lambda x: True):
        print('\t', name)


exercising = initial_exercise
print('\n*************************************************\n')
print('exercising:', exercising.__name__)
exercising()
