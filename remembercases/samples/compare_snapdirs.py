# -*- coding: utf-8 -*-
""" compare two directories with images

    classifies the filenames in
      filename in 1st dir but not in the second
      filename in 2nd dir but not in the first
      filename in both dirs, images ar equals
      filename in both dirs, images are diferent

    target dirs are hardcoded; edit as needed before use
"""
from __future__ import division, print_function, unicode_literals

import os

from remembercases.image_comparison import equal_PIL_robust as is_same_image


def report_comparison_image_directories(dir_1, dir_2,
                                        only_in_1, only_in_2, differents, equals):
    parts = []
    parts.append("Results of comparison of directories with images:")
    parts.append("\tdirectory 1: %s" % dir_1)
    parts.append("\tdirectory 2: %s" % dir_2)

    parts.append("")

    parts.append("Short summary:")
    parts.append("\tonly in directory 1: %d" % len(only_in_1))
    parts.append("\tonly in directory 2: %d" % len(only_in_2))
    parts.append("\tequals: %d" % len(equals))
    parts.append("\tdiferents: %d" % len(diferents))

    parts.append("")

    parts.append("only in directory 1: %d" % len(only_in_1))
    parts.extend(sorted(only_in_1))

    parts.append("")

    parts.append("only in directory 2: %d" % len(only_in_2))
    parts.extend(sorted(only_in_2))

    parts.append("")

    parts.append("equals: %d" % len(equals))
    parts.extend(sorted(equals))

    parts.append("")

    parts.append("diferents: %d" % len(diferents))
    parts.extend(sorted(diferents))

    text = "\n".join(parts)

    return text


dir_1 = r"D:\dev\working\cocos2020\snp_c0.6.6_p1.3.3_py37"
dir_2 = r"D:\dev\working\cocos2020\snp_cb490cc_p1.4.10_py37"

names_in_1 = {name for name in os.listdir(dir_1) if name.endswith(".png")}
names_in_2 = {name for name in os.listdir(dir_2) if name.endswith(".png")}

present_in_both = names_in_1.intersection(names_in_2)
only_in_1 = names_in_1 - present_in_both
only_in_2 = names_in_2 - present_in_both

equals = {e for e in present_in_both if is_same_image(os.path.join(dir_1, e),
                                                      os.path.join(dir_2, e))
          }

diferents = present_in_both - equals

text = report_comparison_image_directories(dir_1, dir_2,
                                           only_in_1, only_in_2, diferents, equals)

print(text)
