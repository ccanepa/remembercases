"""given a dir, detect black snapshots"""
from __future__ import division, print_function, unicode_literals
import os
from remembercases.image_comparison import is_full_black

#dir_1 = r"D:\dev\cocos2019\test\snp_ca58_p82e"

dir_1 = r"D:\tmp\test\snp_cocos66"

black_snapshoots = { name for name in os.listdir(dir_1)
                              if (name.endswith(".png") and
                              is_full_black(os.path.join(dir_1, name)))
                   }
text = '\n'.join(sorted(black_snapshoots))

print("Black Snapshoots in directory:", dir_1)
print("count black snapshoots:'", len(black_snapshoots), '\n')
print(text)
