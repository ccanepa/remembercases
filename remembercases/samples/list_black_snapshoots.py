# -*- coding: utf-8 -*-
"""given a dir, reports all .png's in the dir that are full black"""
from __future__ import division, print_function, unicode_literals
import os

from remembercases.image_comparison import is_full_black


def get_full_black_pngs(target_dir):
    # probably ".PNG" should be taken?
    black_snapshots = {name for name in os.listdir(target_dir)
                       if (name.endswith(".png") and
                           is_full_black(os.path.join(target_dir, name)))
                       }
    return black_snapshots


def rpt_black_snapshots(target_dir, black_snapshots):
    text = '\n'.join(sorted(black_snapshots))

    print("Black Snapshots in directory:", target_dir)
    print("count black snapshots:'", len(black_snapshots), '\n')
    print(text)


def main(target_dir):
    black_snapshots = get_full_black_pngs(target_dir)
    rpt_black_snapshots(target_dir, black_snapshots)


if __name__ == "__main__":
    import sys

    target_dir = "."
    if len(sys.argv) > 1:
        if sys.argv[1] in ["-h", "--help"]:
            print("%s [target_dir]\nDefault target_dir is current dir" % sys.argv[0])
            sys.exit(0)
        else:
            target_dir = sys.argv[1]
    main(target_dir)
