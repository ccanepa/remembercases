from __future__ import division, print_function, unicode_literals

# allows to run tests from the unzipped source / checkout, without installation
import sys
import os
path = os.path.join(os.path.dirname(__file__), '..', '..')
path = os.path.normpath(path)
sys.path.insert(0, path)
#

# meant to run with py.test

import time
import remembercases.proxy as pr
   
def test_proxy_run__timeout():
    default_timeout = 5.0
    start_time = time.time()
    killed, err = pr.proxy_run('dummy_proxy.py', 'no_script.py', ['runforever'], default_timeout)
    if err:
        print('err:', err)
    elapsed = time.time() - start_time
    assert killed
    assert abs((elapsed - default_timeout)/default_timeout) < 0.1

def test_proxy_run__proxy_args():
    default_timeout = 5.0
    script = 'no_script.py'
    proxy_args = ['1', 'with blank']
    killed, err = pr.proxy_run('dummy_proxy.py', script, proxy_args, default_timeout)
    print('err:', err)
    assert not killed
    proxy_args.insert(0, script)
    expected_args = proxy_args
    args_str = err[err.find('@')+1 :].rstrip()
    args = args_str.split(',')
    #strip quotes
    args = [ s[1:-1] for s in args ]
    assert args == expected_args
