# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals

import time
from remembercases.cmds import run_script_proxied_custom as proxy_run


def test_proxy_run__timeout():
    default_timeout = 5.0
    start_time = time.time()
    killed, err = proxy_run('h_dummy_proxy.py', 'no_script.py', ['runforever'],
                            timeout=default_timeout)
    if err:
        print('err:', err)
    elapsed = time.time() - start_time
    assert killed
    assert abs((elapsed - default_timeout)/default_timeout) < 0.1


def test_proxy_run__proxy_args():
    default_timeout = 5.0
    script = 'no_script.py'
    proxy_args = ['1', 'with blank']
    killed, err = proxy_run('h_dummy_proxy.py', script, proxy_args,
                            timeout=default_timeout)
    print('err:', err)
    assert not killed
    proxy_args.insert(0, script)
    expected_args = proxy_args
    args_str = err[err.find('@')+1:].rstrip()
    args = args_str.split(',')
    # strip quotes
    args = [s[1:-1] for s in args]
    assert args == expected_args
