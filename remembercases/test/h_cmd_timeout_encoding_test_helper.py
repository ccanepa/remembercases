# -*- coding: utf-8 -*-
"""
helper for ttest_cmd_timeout.py, outputs a string in a selectable encoding
"""

from __future__ import division, print_function, unicode_literals
import six

import codecs
import sys


def set_stdstreams_encoding(encoding):
    if six.PY2:
        sys.stdout = codecs.getwriter(encoding)(sys.stdout)
        sys.stderr = codecs.getwriter(encoding)(sys.stderr)
    else:
        sys.stdout = open(sys.stdout.fileno(), mode='w', encoding=encoding, buffering=1)
        sys.stderr = open(sys.stderr.fileno(), mode='w', encoding=encoding, buffering=1)


def main():
    if len(sys.argv) != 2:
        raise Exception("Number of arguments must be 1")
    encoding = sys.argv[1]
    assert encoding in ["ascii", "latin1", "utf-8"]
    set_stdstreams_encoding(encoding)
    if encoding == "ascii":
        msg = "All ascii characters"
    else:
        # 'i with acute accent' is an interesting latin1 character because
        # is not a valid 'utf-8' encoding
        msg = "i_with_acute_accent ->í< tail"
    sys.stdout.write("stdout: " + msg)
    sys.stderr.write("stderr: " + msg)


if __name__ == "__main__":
    main()
