# -*- coding: utf-8 -*-
"""
this tests can be run without pytest, which does it own manipulations on
sys.stdout and sys.stderr
"""

from __future__ import division, print_function, unicode_literals
import six

if six.PY2:
    from .cmd_timeout import cmd__uses_event as cmd
else:
    from .cmd_timeout import cmd__py3 as cmd

import sys

def strip_line_ending(s):
    return s.rstrip('\n').rstrip('\r')

def test_cmd_handling_ascii_output():
    """out and err are fine when the target scripts outputs use the ascii charset"""
    cmdline = ["cmd_timeout_test_helper.py", "ascii"]
    killed, returncode, err, out = cmd(cmdline)
    assert type(err) == six.text_type
    assert type(out) == six.text_type
    out = strip_line_ending(out)
#    print("repr(out):", repr(out))
    assert out == "stdout: All ascii characters"
    err = strip_line_ending(err)
#    print("repr(err):", repr(err))
    assert err.endswith("ValueError: stderr: All ascii characters")

def test_cmd_handling_latin1_output_py3():
    """in py3 out and err are fine when the target scripts outputs use the latin1 charset"""
    cmdline = ["cmd_timeout_test_helper.py", "latin1"]
    killed, returncode, err, out = cmd(cmdline)
    assert type(err) == six.text_type
    assert type(out) == six.text_type
    out = strip_line_ending(out)
#    print("repr(out):", repr(out))
    print("Expect to fail in py2 when running under pytest, also misleading pytest diagnostic")
    assert out == "stdout: latin1 'i with acute accent' ->í<"
    err = strip_line_ending(err)
#    print("repr(err):", repr(err))
    assert err.endswith("ValueError: stderr: latin1 'i with acute accent' ->í<")

def test_cmd_handling_latin1_output_py2():
    """
    In py2 a script withs outputs outside the ascii charset may traceback,
    even if the same script completes without exeptions when running directly
    from the command line
    """
    cmdline = ["cmd_timeout_test_helper.py", "latin1"]
    killed, returncode, err, out = cmd(cmdline)
    assert type(err) == six.text_type
    assert type(out) == six.text_type
    out = strip_line_ending(out)
    print("Expect to fail in py3 when running under pytest, also misleading pytest diagnostic")
#    print("repr(out):", repr(out))
    assert out == ""
    err = strip_line_ending(err)
#    print("repr(err):", repr(err))
    assert "stdout" in err
    assert "Traceback" in err
    assert "UnicodeEncodeError" in err

def test_cmd_handling_non_latin1_output():
    """
    In py2 and py3 a script withs outputs outside the ascii charset may traceback,
    even if the same script completes without exeptions when running directly
    from the command line
    """
    cmdline = ["cmd_timeout_test_helper.py", "non_latin1"]
    killed, returncode, err, out = cmd(cmdline)
    assert type(err) == six.text_type
    assert type(out) == six.text_type
    out = strip_line_ending(out)
#    print("repr(out):", repr(out))
    assert out == ""
    err = strip_line_ending(err)
#    print("repr(err):", repr(err))
    assert "stdout" in err
    assert "Traceback" in err
    assert "UnicodeEncodeError" in err


if __name__ == "__main__":
    try:
        test_cmd_handling_ascii_output()
        print(">>> test_cmd_handling_ascii_output_py3 PASS.\n")
    except Exception:
        print(">>> test_cmd_handling_ascii_output_py3 FAILS.\n")

    if six.PY2:
        print(">>> test_cmd_handling_latin1_output_py3 SKIPPED.\n")
    else:
        try:
            test_cmd_handling_latin1_output_py3()
            print(">>> test_cmd_handling_latin1_output_py3 PASS.\n")
        except Exception:
            print(">>> test_cmd_handling_latin1_output_py3 FAILS.\n")
            
    if six.PY2:
        try:
            test_cmd_handling_latin1_output_py2()
            print(">>> test_cmd_handling_latin1_output_py2 PASS.\n")
        except Exception:
            print(">>> test_cmd_handling_latin1_output_py2 FAILS.\n")
    else:
        print(">>> test_cmd_handling_latin1_output_py2 SKIPPED.\n")
        
    try:
        test_cmd_handling_non_latin1_output()
        print(">>> test_cmd_handling_non_latin1_output PASS.\n")
    except Exception:
        print(">>> test_cmd_handling_non_latin1_output FAILS.\n")
        
