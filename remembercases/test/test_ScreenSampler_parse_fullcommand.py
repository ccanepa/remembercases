# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals

import pytest

import remembercases.snapshot_taker as st


def params_for_test_parse_fullcommand():
    param_names = ['fullcmd', 'diag_start', 'expected_parts']

    cases = {
        's_ok':     ('s', '', ['s']),
        's_Eti01':  ('s 1', 'Eti01', '-'),
        't_ok':     ('t 1.2', '', ['t', 1.2]),
        't_Eti01':  ('t', 'Eti01', '-'),
        't_Eti02':  ('t a', 'Eti02', '-'),
        'dt_ok':    ('dt 1.3', '', ['dt', 1.3]),
        'dt_Eti01': ('dt', 'Eti01', '-'),
        'dt_Eti02': ('dt a', 'Eti02', '-'),
        'q_ok':     ('q', '', ['q']),
        'q_Eti01':  ('q 1', 'Eti01', '-'),
        'f_ok':     ('f 10 0.1', '', ['f', 10, 0.1]),
        }

    params = [cases[name] for name in cases]
    sufixes_parametrized_tests = [name for name in cases]
    # return tuple must match pytest.mark.parametrize signature; for pytest
    # 4.6.11 which is the last supporting py27
    # it is Metafunc.parametrize(argnames, argvalues, indirect=False, ids=None, scope=None)
    return param_names, params, False, sufixes_parametrized_tests


# (argnames, argvalues, indirect=False, ids=None, scope=None)
@pytest.mark.parametrize(*params_for_test_parse_fullcommand())
def test_parse_fullcommand(fullcmd, diag_start, expected_parts):
    diagnostics, parsed_command = st.ScreenSampler._parse_fullcommand(fullcmd)
    if diag_start == '':
        # expected success
        assert diagnostics == ''
        assert expected_parts == parsed_command
    else:
        # expected failure
        assert diagnostics.startswith(diag_start)
