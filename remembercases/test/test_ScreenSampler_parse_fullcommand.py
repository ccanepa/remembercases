from __future__ import print_function
from __future__ import division

# allows to run tests from the unzipped source / checkout, without installation
import sys
import os
path = os.path.join(os.path.dirname(__file__), '..', '..')
path = os.path.normpath(path)
sys.path.insert(0, path)
#

# meant to run with py.test

import remembercases.snapshot_taker as st

def pytest_generate_tests(metafunc):
    param_names = ['fullcmd', 'diag_start', 'expected_parts']
    
    cases = {
        's_ok':     ('s', '', ['s']),
        's_Eti01':  ('s 1', 'Eti01', '-'),
        't_ok':     ('t 1.2', '', ['t', 1.2]),
        't_Eti01':  ('t', 'Eti01', '-'),
        't_Eti02':  ('t a', 'Eti02', '-'),
        'dt_ok':    ('dt 1.3', '', ['dt', 1.3]),
        'dt_Eti01': ('dt', 'Eti01', '-'),
        'dt_Eti02': ('dt a', 'Eti02', '-'),
        'q_ok':     ('q', '', ['q']),
        'q_Eti01':  ('q 1', 'Eti01', '-'),
        'f_ok':     ('f 10 0.1', '', ['f', 10, 0.1]),
        }
    
    scenarios = {}
    for name in cases:
        scenarios[name] = dict(zip(param_names, cases[name]))
    for k in scenarios:
        metafunc.addcall(id=k, funcargs=scenarios[k])

def test_parse_fullcommand(fullcmd, diag_start, expected_parts):
    diagnostics, parsed_command = st.ScreenSampler._parse_fullcommand(fullcmd)
    if diag_start == '':
        # expected succes
        assert diagnostics == ''
        assert expected_parts == parsed_command
    else:
        #expected failure
        assert diagnostics.startswith(diag_start)
