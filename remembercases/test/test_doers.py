# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals
import six  # noqa: F401

import pytest
import os
import tempfile
import shutil
import remembercases.doers as th


# >>> files_from_dir >>>>>>>>>>>>>>>>>>>>

def test_files_from_dir__path_not_found():
    with pytest.raises(ValueError):
        iter(th.files_from_dir('z1@@@1z'))


def test_files_from_dir__path_not_a_directory():
    with pytest.raises(ValueError):
        iter(th.files_from_dir(__file__))


def test_files_from_dir__finds_existing_directory():
    def fn_fname_test_py(filename):
        return filename.startswith('test_') and filename.endswith('.py')
    should_see = {'test_1_ok.py', 'test_2_ok.py'}
    should_not_see = {'test_not_ok.pyc', 'not_ok.py'}
    test_dir = tempfile.mkdtemp(suffix='tmp')
    print('test_dir:', test_dir)

    toCreate = should_see.union(should_not_see)
    for shortname in toCreate:
        open(os.path.join(test_dir, shortname), 'wb').close()

    files_seen = [fname for fname in th.files_from_dir(test_dir, fn_fname_test_py)]
    should_see = [os.path.join(test_dir, s) for s in should_see]
    assert len(files_seen) == len(should_see)
    assert set(should_see) == set(files_seen)

    shutil.rmtree(test_dir)


# >>> clean_line >>>>>>>>>>>>>>>>>>>>

def test_clean_line__empty_line():
    line = ''
    assert th.clean_line(line) == ''
    assert th.clean_line(line, end_mark='#') == ''


def test_clean_line__whitespace_line():
    line = ' \t\r'
    assert th.clean_line(line) == ''
    assert th.clean_line(line, end_mark='#') == ''


def test_clean_line__only_discardable_line():
    line = '#foo \t\r'
    assert th.clean_line(line, end_mark='#') == ''
    line = ' \t;foo \t\r'
    assert th.clean_line(line, end_mark=';') == ''


def test_clean_line__just_payload():
    line = 'one.py'
    assert th.clean_line(line) == line
    assert th.clean_line(line, end_mark='#') == line


def test_clean_line__payload_with_whitespace():
    payload = 'one two.py'
    line = ' \t' + payload + ' \t'
    assert th.clean_line(line) == payload
    assert th.clean_line(line, end_mark='#') == payload


def test_clean_line__payload_with_discardable():
    payload = 'one two.py'
    discardable = '#to discard'
    line = payload + ' \t' + discardable
    assert th.clean_line(line, end_mark='#') == payload

    discardable = '#to discard'
    line = ' \t' + payload + ' \t' + discardable
    assert th.clean_line(line, end_mark='#') == payload


# >>> scripts_names_from_text >>>>>>>>>>>>>>>>>>>>

def test_scripts_names_from_text__all_whitespace_text():
    text = '\n' + ' \n' + ' \t\r\n'
    assert len(th.scripts_names_from_text(text)) == 0


def test_scripts_names_from_text__all_lines_whith_payload():
    payloads = {'one', 'two', 'with blank'}
    text = '\n'.join(payloads)
    assert th.scripts_names_from_text(text) == payloads


def test_scripts_names_from_text__some_lines_discardables():
    payloads = {'one', 'two', 'with blank'}
    text = '\n'.join(payloads) + '\n' + '\n\r' + '\n\t'
    assert th.scripts_names_from_text(text) == payloads


def test_pprint_to_string():
    obj = 1
    s = th.pprint_to_string(obj)
    assert s == '1\n'


def test_join_sorted():
    strings = ['1', '3', '2']
    expected = "1\n2\n3"
    assert th.join_sorted(strings) == expected


# >>> helpers

def test_hasher():
    h = th.HasherMD5()
    text = "xyz"
    hash_in_py35 = "d16fb36f0911f878998c136191af705e"

    h.update(text)

    assert h.hash_as_text() == hash_in_py35
