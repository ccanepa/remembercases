# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals

import pytest

import remembercases.snapshot_taker as st

# With the actual code, parse failures will raise ValueError


def params_for_test_sampler():
    param_names = ['test_info', 'diag_start', 'expected_snapshots']

    cases = {
        # case name: params
        's_q__ok': ('s, q', '', ['test_dummy_00.000.png']),
        'dt_t_s_q__ok': ('dt 0.1, t 1.0, s, q', '', ['test_dummy_01.000.png']),
        'dt_Eti01_less params': ('dt , q', 'Eti01', []),
        's_Eti01_extra_param': ('s 1, q', 'Eti01', []),
        't_Eti02_type_mismatch': ('t aa',  'Eti02', []),
        'Eti05_unknown_command': ('bb 1', 'Eti05', []),
        't_Eti03_non_ascending_time': ('t 1.0, t 0.5', 'Eti03', []),
        'dt_Eti04_non_ascending_time': ('dt 0.0, q', 'Eti04', []),
        'f_Eti06_non_positive_count_frames': ('f 0 0.1', 'Eti06', []),
        'f_Eti07_non_ascending_time': ('f 2 0.0', 'Eti07', []),
        }

    params = [cases[name] for name in cases]
    sufixes_parametrized_tests = [name for name in cases]
    # return tuple must match pytest.mark.parametrize signature; for pytest
    # 4.6.11 which is the last supporting py27
    # it is Metafunc.parametrize(argnames, argvalues, indirect=False, ids=None, scope=None)
    return param_names, params, False, sufixes_parametrized_tests


# (argnames, argvalues, indirect=False, ids=None, scope=None)
@pytest.mark.parametrize(*params_for_test_sampler())
def test_sampler(test_info, diag_start, expected_snapshots):
    script_name = 'test_dummy.py'
    rec = st._ValidationHelper()
    sampler, diagnostic = st.ScreenSampler.sampler(test_info, script_name,
                                    fn_quit=rec.fn_quit,
                                    fn_take_snapshot=rec.fn_take_snapshot,
                                    snapshots_dir=None)
    if diag_start == '':
        assert diagnostic == ''
        assert sampler.diagnostic == ''
    else:
        assert diagnostic.startswith(diag_start)
