#!/usr/bin/env python
"""helper to test cmd_timeout.py
Sends to stdout int(sys.argv[1]) bytes, with the pattern
'B> b...b <E'
Then
 - if len(sys.argv)==2, the script ends.
 - else, it enters an infinite NOP loop
"""
from __future__ import division, print_function, unicode_literals

import sys
import time


def main():
    prefix = 'B> '
    tail = ' <E'
    output = prefix + "b" * (int(sys.argv[1])-(len(prefix)+len(tail))) + tail
    assert len(output) == int(sys.argv[1])
    sys.stdout.write(output)

    if len(sys.argv) > 2:
        while 1:
            time.sleep(1)


if __name__ == "__main__":
    main()
