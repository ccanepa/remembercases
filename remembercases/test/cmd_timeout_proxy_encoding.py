"""
helper for cmd_timeout__explore_output_capture.py

The initial intention was to force an encoding for communicate, but no luck
with that
"""
from __future__ import division, print_function, unicode_literals
import six

import sys

import cmd_timeout_target_encoding as target

# from http://stackoverflow.com/questions/4374455/how-to-set-sys-stdout-encoding-in-python-3

##def set_std_encoding_py2():
##    sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
##    sys.stdout.encoding = "utf-8"
##    sys.stderr = codecs.getwriter("utf-8")(sys.stderr)
##    sys.stderr.encoding = "utf-8"
##
##def set_std_encoding_py3():
##    sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())
##    sys.stderr = codecs.getwriter("utf-8")(sys.stderr.detach())
##
##if hasattr(sys.stdout, 'encoding'):
##    encoding = sys.stdout.encoding
##else:
##    encoding = "<not present>"
###print("sys.stdout.encoding:", encoding)
##
##if six.PY2:
##    set_std_encoding = set_std_encoding_py2
##else:
##    set_std_encoding = set_std_encoding_py3
##
##if hasattr(sys.stdout, 'encoding'):
##    encoding = sys.stdout.encoding
##else:
##    encoding = "<not present>"
###print("sys.stdout.encoding:", encoding)
### ^ gives None in py 2.6.6
##
##if hasattr(sys.stderr, 'encoding'):
##    encoding = sys.stderr.encoding
##else:
##    encoding = "<not present>"
###print("sys.stderr.encoding:", encoding)
### ^ gives None in py 2.6.6

target.main()
