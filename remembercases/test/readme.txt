test_cmd_timeout is not an unit test, it is an utility to report how good are different methods of using subprocess, with timeout and output capture.
In windows some methods don't capture the full output.

The other test_* are unit tests to  run with pytest.
