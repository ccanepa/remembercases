Run tests by
	cd remembercases\remembercases\test
	pytest 
(there will be errors if running grom outside the test/ directory)
 
test_*.py: test meant to run under pytest
explore_*.py: reports info related to a theme
h_*.py: helpers meant to be used by the scripts above

Note: 4.6.11 is the last pytest supporting py27

explore_cmd_timeout__output_capture.py:
 - not an automated test (no assertions). Exercises 'run_script.py' with
   different parameters and reports a table of behavior observed.
   It is about if stdout - stderr can be full captured even when tracebacks
   or timeouts happen (In windows some methods tried didn't capture the full
   output, see v0.3 or lower for more details)
   Can take different implementations of run_script if desired
