# -*- coding: utf-8 -*-
"""
helper for cmd_timeout__explore_output_capture.py
"""

from __future__ import division, print_function, unicode_literals

def main():
    # output characters that will traceback if cmd_timeout does not
    # appropiatelly handle encodings
    # strings must be in sync with test_cmd_timeout
    print("stdout: í <- that was i with accute accent")
    raise ValueError("stderr: í <- that was i with accute accent")

