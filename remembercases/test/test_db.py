# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals

# allows to run tests from the unzipped source / checkout, without installation
import sys
import os
import io
import copy

import pytest

import remembercases.db as dbm


def test_iter_empty_db():
    db = dbm.TestbedEntityPropDB()
    it = iter(db)
    with pytest.raises(StopIteration):
        next(it)


def test_add_testbed__different_testbeds():
    db = dbm.TestbedEntityPropDB()
    testbeds = ['one', 'two', 'three']
    for e in testbeds:
        db.add_testbed(e, '')
    known_testbeds = [e for e in db]
    print(known_testbeds)
    testbeds.sort()
    known_testbeds.sort()
    assert known_testbeds == testbeds


def test_add_testbed__duplicate_testbeds():
    # should raise and not modify the db at all
    db = dbm.TestbedEntityPropDB()
    testbed = 'one'
    db.add_testbed(testbed, '')
    with pytest.raises(dbm.ErrorDuplicatedTestbed):
        db.add_testbed(testbed, '')


def test_good_testbed__known_testbed():
    db = dbm.TestbedEntityPropDB()
    testbeds = ['one', 'two', 'three']
    for e in testbeds:
        db.add_testbed(e, '')
    for e in testbeds:
        assert e == db.good_testbed(e)
    db.set_default_testbed('two')
    assert db.good_testbed(None) == 'two'


def test_good_testbed__unknown_testbed():
    db = dbm.TestbedEntityPropDB()
    testbed = 'one'
    db.add_testbed(testbed, '')
    with pytest.raises(dbm.ErrorUnknownTestbed):
        db.good_testbed('two')


def test_set_default_testbed__unknown_testbed():
    db = dbm.TestbedEntityPropDB()
    testbed = 'one'
    db.add_testbed(testbed, '')
    with pytest.raises(dbm.ErrorUnknownTestbed):
        db.set_default_testbed('two')


def test_clone_testbed__src_ne_dst():
    db = dbm.TestbedEntityPropDB()
    testbed = 'one'
    db.add_testbed(testbed, '')
    group_props = ['g_prop1', 'g_prop2', 'g_prop3']
    db.add_group('tgroup', group_props, allow_redefine=False,
                 addprops=True, testbed='one')
    entities = ['a', 'b', 'c', 'd']
    for e in entities:
        db.add_entity(e, 'one')
        for prop in group_props:
            db.set_prop_value(e, prop, (e, prop), 'one')
    dst = 'new'
    candidates = ['a', 'c', 'd', 'x']
    db.clone_testbed('one', dst, candidates)
    # ents not in candidates are not present in the clone
    with pytest.raises(dbm.ErrorUnknownEntity):
        db.get_propdict('b', dst)

    # ents in candidates but not in src testbed don't appear in the clone
    with pytest.raises(dbm.ErrorUnknownEntity):
        db.get_propdict('x', dst)

    # ents both in candidates and src testbed appears in the clone, with same
    # propdict
    common = ['a', 'c', 'd']
    db.del_entity('b', 'one')
    for e in common:
        assert db.get_propdict(e, dst) == db.get_propdict(e, 'one')


def test_del_testbed__no_default():
    db = dbm.TestbedEntityPropDB()
    testbed = 'one'
    db.add_testbed(testbed, '')
    db.del_testbed(testbed)
    with pytest.raises(dbm.ErrorUnknownTestbed):
        db.good_testbed(testbed)


def test_del_testbed__default():
    db = dbm.TestbedEntityPropDB()
    testbed = 'one'
    db.add_testbed(testbed, '')
    db.add_testbed('two', '')
    db.set_default_testbed('one')
    db.del_testbed()
    with pytest.raises(dbm.ErrorUnknownTestbed):
        db.good_testbed(testbed)
    old_testbed = db.set_default_testbed('two')
    assert old_testbed is None


def test_rename_testbed__equals():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    testbed_data = db.db['one']
    db.rename_testbed('one', 'one')
    assert testbed_data is db.db['one']


def test_rename_testbed__rejects_None():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    with pytest.raises(ValueError):
        db.rename_testbed('one', None)
    with pytest.raises(ValueError):
        db.rename_testbed(None, 'one')


def test_rename_testbed__rejects_unknown_old():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    with pytest.raises(dbm.ErrorUnknownTestbed):
        db.rename_testbed('unknown', 'new')


def test_rename_testbed__rejects_duplicate_new():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    db.add_testbed('two', '')
    with pytest.raises(dbm.ErrorDuplicatedTestbed):
        db.rename_testbed('one', 'two')


def test_rename_testbed__old_is_default():
    db = dbm.TestbedEntityPropDB()
    old = "old"
    db.add_testbed(old, '')
    db.set_default_testbed(old)
    old_testbed_data = db.db[old]
    # sanity
    assert db.default_testbed is old
    new = "new"
    res = db.rename_testbed(old, new)
    assert db.db[new] is old_testbed_data
    assert res is new
    assert res is db.default_testbed


def test_rename_testbed__old_is_not_default():
    db = dbm.TestbedEntityPropDB()
    other = 'other'
    db.add_testbed(other, '')
    db.set_default_testbed(other)
    # sanity
    assert db.default_testbed is other
    old = "old"
    db.add_testbed(old, '')
    old_testbed_data = db.db[old]
    new = "new"
    res = db.rename_testbed(old, new)
    assert db.db[new] is old_testbed_data
    assert res is other
    assert res is db.default_testbed


def test_add_prop__applies_to_target_testbed():
    db = dbm.TestbedEntityPropDB()
    testbed_one = 'one'
    db.add_testbed(testbed_one, '')
    propname_one = testbed_one + ' prop'
    db.add_prop(propname_one, testbed=testbed_one)

    testbed_two = 'two'
    db.add_testbed(testbed_two, '')
    propname_two = testbed_two + ' prop'
    db.set_default_testbed(testbed_two)
    db.add_prop(propname_two)

    assert db.good_prop(propname_one, testbed_one) is None
    with pytest.raises(dbm.ErrorUnknownProp):
        db.good_prop(propname_two, testbed_one)
    with pytest.raises(dbm.ErrorUnknownProp):
        db.good_prop(propname_one, testbed_two)
    assert db.good_prop(propname_two, testbed_two) is None


def test_add_group__addprops_true__no_prop_collision():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    db.add_prop('known', testbed='one')
    group_props = ['g_prop1', 'g_prop2', 'g_prop3']
    db.add_group('tgroup', group_props, allow_redefine=False,
                 addprops=True, testbed='one')
    for k in group_props:
        assert db.good_prop(k, 'one') is None


def test_add_group__addprops_true__with_prop_collision():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    db.add_prop('g_prop2', testbed='one')
    group_props = ['g_prop1', 'g_prop2', 'g_prop3']
    old_dbdb = copy.deepcopy(db.db)
    # should raise and not modify the db at all
    with pytest.raises(dbm.ErrorDuplicatedProp):
        db.add_group('tgroup', group_props, allow_redefine=False,
                     addprops=True, testbed='one')
    assert db.db == old_dbdb


def test_add_group__addprops_false__missing_prop():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    db.add_prop('known', testbed='one')
    group_props = ['known', 'g_prop2']
    old_dbdb = copy.deepcopy(db.db)
    # should raise and not modify the db at all
    with pytest.raises(dbm.ErrorUnknownProp):
        db.add_group('tgroup', group_props, allow_redefine=False,
                     addprops=False, testbed='one')
    assert db.db == old_dbdb


def test_add_group__addprops_false__not_missing_prop():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    group_props = ['g_prop1', 'g_prop2', 'g_prop3']
    for k in group_props:
        db.add_prop(k, testbed='one')
    # don't raises
    db.add_group('tgroup', group_props, allow_redefine=False,
                 addprops=False, testbed='one')


def test_set_prop_value__known_prop():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    db.add_prop('known', testbed='one')
    entity = 'file1'
    db.add_entity(entity, testbed='one')
    prop_value = 'zzz'
    db.set_prop_value(entity, 'known', prop_value, testbed='one')
    assert prop_value == db.get_prop_value(entity, 'known', testbed='one')


def test_set_prop_value__unknown_prop():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    db.add_prop('known', testbed='one')
    entity = 'file1'
    db.add_entity(entity, testbed='one')
    prop_value = 'zzz'
    with pytest.raises(dbm.ErrorUnknownProp):
        db.set_prop_value(entity, 'unknown', prop_value, testbed='one')


def test_set_prop_value__list():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    db.add_prop('present', testbed='one')
    entity = 'file1'
    db.add_entity(entity, testbed='one')
    prop_value = ['zzz']
    db.set_prop_value(entity, 'present', prop_value, testbed='one')
    assert prop_value == db.get_prop_value(entity, 'present', testbed='one')
    prop_value[0] = 'k'
    assert ['zzz'] == db.get_prop_value(entity, 'present', testbed='one')


def test_get_prop_value__list():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    db.add_prop('present', testbed='one')
    entity = 'file1'
    db.add_entity(entity, testbed='one')
    prop_value = ['zzz']
    db.set_prop_value(entity, 'present', prop_value, testbed='one')
    assert prop_value == db.get_prop_value(entity, 'present', testbed='one')
    value_from_get = db.get_prop_value(entity, 'present', testbed='one')
    value_from_get[0] = 'k'
    assert ['zzz'] == db.get_prop_value(entity, 'present', testbed='one')


def test_get_groupdict__partial_groupdict():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    group_props = ['g_prop1', 'g_prop2', 'g_prop3']
    for k in group_props:
        db.add_prop(k, testbed='one')
    db.add_group('tgroup', group_props, allow_redefine=False,
                 addprops=False, testbed='one')
    entity = 'file1'
    db.add_entity(entity, testbed='one')
    db.add_prop('other', testbed='one')
    db.set_prop_value(entity, 'other', 9, testbed='one')
    db.set_prop_value(entity, 'g_prop2', 7, testbed='one')
    assert db.get_groupdict(entity, 'tgroup', testbed='one') == {'g_prop2': 7}


def test_del_groupdict__partial_groupdict():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    group_props = ['g_prop1', 'g_prop2', 'g_prop3']
    entity = 'file1'
    db.add_entity(entity, testbed='one')
    db.add_prop('other', testbed='one')
    db.set_prop_value(entity, 'other', 9, testbed='one')
    old_propdict = db.get_propdict(entity, testbed='one')
    for k in group_props:
        db.add_prop(k, testbed='one')
    db.add_group('tgroup', group_props, allow_redefine=False,
                 addprops=False, testbed='one')
    db.set_prop_value(entity, 'g_prop2', 7, testbed='one')
    db.del_groupdict(entity, 'tgroup', testbed='one')
    assert old_propdict == db.get_propdict(entity, testbed='one')


def test_set_groupdict__partial_groupdict():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    group_props = ['g_prop1', 'g_prop2', 'g_prop3']
    for k in group_props:
        db.add_prop(k, testbed='one')
    db.add_group('tgroup', group_props, allow_redefine=False,
                 addprops=False, testbed='one')
    entity = 'file1'
    db.add_entity(entity, testbed='one')
    db.add_prop('other', testbed='one')
    db.set_prop_value(entity, 'other', 9, testbed='one')
    db.set_groupdict(entity, 'tgroup', {'g_prop2': 7}, testbed='one')
    assert db.get_propdict(entity, testbed='one') == {'g_prop2': 7, 'other': 9}


def test_persist():
    db = dbm.TestbedEntityPropDB()
    testbeds = ['one', 'two', 'three']
    for t in testbeds:
        db.add_testbed(t, '')
    for t in testbeds:
        prop = 'prop_' + t
        db.add_prop('prop_' + t, t)
        for prefix in ['ent0', 'ent1']:
            entity = prefix + t
            db.add_entity(entity, t)
            db.set_prop_value(entity, prop, prefix + '_prop_value', t)

    outfile = io.BytesIO()
    db.save(outfile)

    infile = io.BytesIO(outfile.getvalue())
    new = dbm.TestbedEntityPropDB.new_from_file(infile)
    assert new.db == db.db


def test_notes():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    text = "working now in initial recon"
    db.set_notes(text, testbed='one')
    assert db.get_notes(testbed='one') == text


def test_log():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    assert db.export_log('one') == ''

    text1 = 'text1'
    db.log(text1, testbed='one')
    assert db.export_log(testbed='one') == dbm.LOG_SEPARATOR + text1

    text2 = 'text2'
    db.log(text2, testbed='one')
    assert (db.export_log(testbed='one') ==
            dbm.LOG_SEPARATOR.join(['', text1, text2]))


def test_get_propdict():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    entity = 'file1'
    db.add_entity(entity, testbed='one')
    d = {}
    assert db.get_propdict(entity, testbed='one') == d

    props = ['a', 'b', 'c', 'd']
    for i, p in enumerate(props):
        db.add_prop(p, testbed='one')
        db.set_prop_value(entity, p, i, testbed='one')
        d[p] = i
        assert db.get_propdict(entity, testbed='one') == d


def test_get_propdict__list():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    db.add_prop('present', testbed='one')
    entity = 'file1'
    db.add_entity(entity, testbed='one')
    prop_value = ['zzz']
    db.set_prop_value(entity, 'present', prop_value, testbed='one')
    d = db.get_propdict(entity, testbed='one')
    d['present'][0] = 'k'
    assert ['zzz'] == db.get_prop_value(entity, 'present', testbed='one')


def test_set_propdict__unknown_props():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    db.add_prop('present', testbed='one')
    entity = 'file1'
    db.add_entity(entity, testbed='one')
    db.set_prop_value(entity, 'present', 'uuu', testbed='one')
    old_propdict = db.get_propdict(entity, testbed='one')
    d = {'a': 1, 'b': 2}
    with pytest.raises(dbm.ErrorUnknownProp):
        db.set_propdict(entity, d, testbed='one')
    assert old_propdict == db.get_propdict(entity, testbed='one')


def test_set_propdict__known_props():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    db.add_prop('present', testbed='one')
    entity = 'file1'
    db.add_entity(entity, testbed='one')
    db.set_prop_value(entity, 'present', 'uuu', testbed='one')
    d = {'a': 1, 'b': 2}
    for p in d:
        db.add_prop(p, testbed='one')
    db.set_propdict(entity, d, testbed='one')
    assert {'a': 1, 'b': 2} == db.get_propdict(entity, testbed='one')


def test_set_propdict__list():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    db.add_prop('present', testbed='one')
    entity = 'file1'
    db.add_entity(entity, testbed='one')
    d = {'present': ['zzz']}
    db.set_propdict(entity, d, testbed='one')
    d['present'][0] = 'k'
    assert {'present': ['zzz']} == db.get_propdict(entity, testbed='one')


def test_del_entity_props():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    entity = 'file1'
    db.add_entity(entity, testbed='one')
    props = ['a', 'b', 'c', 'd']
    for p in props:
        db.add_prop(p, testbed='one')
        db.set_prop_value(entity, p, p + 'z', testbed='one')
    db.del_entity_props(entity, ['a', 'd'], testbed='one')
    assert db.get_propdict(entity, testbed='one') == {'b': 'bz', 'c': 'cz'}

    db.del_entity_props(entity, ['k'], testbed='one')
    assert db.get_propdict(entity, testbed='one') == {'b': 'bz', 'c': 'cz'}


def test_canonical_fname__abspath():
    if sys.platform == 'win32':
        basepath = r"c:\repo"
        filename = r"c:\repo\test\test_base.py"
        expected = r"test/test_base.py"
        db = dbm.TestbedEntityPropDB()
        db.add_testbed('one', basepath)
        canonical_fname = db.canonical_fname(filename, testbed='one')
        assert canonical_fname == expected
    else:
        basepath = "/fake"
        filename = "/fake/test/test_base.py"
        expected = "test/test_base.py"
        db = dbm.TestbedEntityPropDB()
        db.add_testbed('one', basepath)
        canonical_fname = db.canonical_fname(filename, testbed='one')
        assert canonical_fname == expected


def test_fname_from_canonical__abspath():
    if sys.platform == 'win32':
        basepath = r"c:\repo"
        canonical_filename = r"test/test_base.py"
        expected = r"c:\repo\test\test_base.py"
        db = dbm.TestbedEntityPropDB()
        db.add_testbed('one', basepath)
        f_from_canonical = db.fname_from_canonical(canonical_filename, testbed='one')
        assert (os.path.abspath(f_from_canonical).split('\\') ==
                os.path.abspath(expected).split('\\'))
    else:
        basepath = "/fake"
        canonical_filename = "test/test_base.py"
        expected = "/fake/test/test_base.py"
        db = dbm.TestbedEntityPropDB()
        db.add_testbed('one', basepath)
        f_from_canonical = db.fname_from_canonical(canonical_filename, testbed='one')
        assert (os.path.abspath(f_from_canonical).split('/') ==
                os.path.abspath(expected).split('/'))


def test_canonical_fname__relpath():
    if sys.platform == 'win32':
        basepath = r"..\.."
        filename = r"..\..\test\test_base.py"
        expected = r"test/test_base.py"
        db = dbm.TestbedEntityPropDB()
        db.add_testbed('one', basepath)
        canonical_fname = db.canonical_fname(filename, testbed='one')
        assert canonical_fname == expected
    else:
        basepath = "../.."
        filename = "../../test/test_base.py"
        expected = "test/test_base.py"
        db = dbm.TestbedEntityPropDB()
        db.add_testbed('one', basepath)
        canonical_fname = db.canonical_fname(filename, testbed='one')
        assert canonical_fname == expected


def test_fname_from_canonical__relpath():
    if sys.platform == 'win32':
        basepath = r"..\.."
        canonical_filename = r"test/test_base.py"
        expected = r"..\..\test\test_base.py"
        db = dbm.TestbedEntityPropDB()
        db.add_testbed('one', basepath)
        f_from_canonical = db.fname_from_canonical(canonical_filename, testbed='one')
        assert f_from_canonical == expected
    ##        assert (os.path.abspath(f_from_canonical).split('\\') ==
    ##                os.path.abspath(expected).split('\\'))
    else:
        basepath = "../.."
        canonical_filename = "test/test_base.py"
        expected = "../../test/test_base.py"
        db = dbm.TestbedEntityPropDB()
        db.add_testbed('one', basepath)
        f_from_canonical = db.fname_from_canonical(canonical_filename, testbed='one')
        assert f_from_canonical == expected
##        assert (os.path.abspath(f_from_canonical).split('\\') ==
##                os.path.abspath(expected).split('\\'))


def test_num_entities():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    assert db.num_entities(testbed='one') == 0
    for n, entity in enumerate(['a', 'b', 'c']):
        db.add_entity(entity, testbed='one')
        assert db.num_entities(testbed='one') == (n + 1)


def test_entities__wider_defaults():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    initial_entities = ['a', 'b', 'c', 'd']
    for entity in initial_entities:
        db.add_entity(entity, testbed='one')

    allowed_entities, unknowns = db.entities(fn_allow=None,
                                             candidates=None,
                                             testbed='one')

    assert allowed_entities == set(initial_entities)
    assert unknowns == set()


def test_entities__default_fn_allow():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    initial_entities = ['a', 'b', 'c', 'd']
    candidates = ['b', 'd']
    for entity in initial_entities:
        db.add_entity(entity, testbed='one')

    allowed_entities, unknowns = db.entities(fn_allow=None,
                                             candidates=candidates,
                                             testbed='one')

    assert allowed_entities == set(candidates)
    assert unknowns == set()


def test_entities__default_candidates():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    db.add_prop('num', testbed='one')
    initial_entities = ['a', 'b', 'c', 'd']
    for n, entity in enumerate(initial_entities):
        db.add_entity(entity, testbed='one')
        db.set_prop_value(entity, 'num', n, testbed='one')

    def fn_allow_even_num(propdict):
        return 'num' in propdict and propdict['num'] % 2 == 0

    expect_entities = {'a', 'c'}

    allowed_entities, unknowns = db.entities(fn_allow=fn_allow_even_num,
                                             candidates=None,
                                             testbed='one')

    assert allowed_entities == expect_entities
    assert unknowns == set()


def test_entities__explicit_fn_allow_and_candidates():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    db.add_prop('num', testbed='one')
    initial_entities = ['a', 'b', 'c', 'd']
    for n, entity in enumerate(initial_entities):
        db.add_entity(entity, testbed='one')
        db.set_prop_value(entity, 'num', n, testbed='one')

    def fn_allow_even_num(propdict):
        return 'num' in propdict and propdict['num'] % 2 == 0

    unknown_entities = {'u', 'v'}
    candidates = set(initial_entities) | unknown_entities
    candidates.discard('c')
    expect_entities = {'a'}

    allowed_entities, unknowns = db.entities(fn_allow=fn_allow_even_num,
                                             candidates=candidates,
                                             testbed='one')

    assert allowed_entities == expect_entities
    assert unknowns == unknown_entities


def test_del_entity__present():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    entity = 'file1'
    db.add_entity(entity, testbed='one')
    db.del_entity('file1', testbed='one')
    allowed_entities, unknown = db.entities(fn_allow=None, candidates=None,
                                            testbed='one')
    assert len(allowed_entities) == 0 and len(unknown) == 0


def test_del_entity__absent():
    db = dbm.TestbedEntityPropDB()
    db.add_testbed('one', '')
    db.del_entity('file1', testbed='one')
    allowed_entities, unknown = db.entities(fn_allow=None, candidates=['file1'],
                                            testbed='one')
    assert len(allowed_entities) == 0 and unknown == {'file1'}
