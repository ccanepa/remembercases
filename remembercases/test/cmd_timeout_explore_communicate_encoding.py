# -*- coding: utf-8 -*-
"""
Things learned

1. (in win xp) subprocess communicate seems to use "latin1" encoding
2. (in win xp) subprocess communicate seems to use "ascii" encoding
3. A script running under pytest in python 3 sees sys.stdout.encoding as "utf-8"
4. As a consequence, if the tests runs trough a if __name__ == "__main__" it can
give different results than running under pytest
"""
from __future__ import division, print_function, unicode_literals

from remembercases.cmd_timeout import cmd__uses_event as cmd

import sys
print('encodings outside the tests:')
print('sys.stdout.encoding:', sys.stdout.encoding)
print('sys.stderr.encoding:', sys.stderr.encoding)

def strip_line_ending(s):
    return s.rstrip('\n').rstrip('\r')

# (win xp) fails in py 3.3.1 and py 2.6.6 (in 3 it miss-translates accute accented i)
# both for if __name__ and pytest runs
def test_encoding__sys_std_encoding():
    print('encodings into the test:')
    print('sys.stdout.encoding:', sys.stdout.encoding)
    print('sys.stderr.encoding:', sys.stderr.encoding)
    stdout_encoding = sys.stdout.encoding
    stderr_encoding = sys.stderr.encoding
    cmdline = ["cmd_timeout_proxy_encoding.py"]
    killed, returncode, err, out = cmd(cmdline)
    out = out.decode(stdout_encoding)
    out = strip_line_ending(out)
    err = err.decode(stderr_encoding)
    err = strip_line_ending(err)
##    print()
##    print("repr(err):", repr(err))
##    print()
##    print("repr(out):", repr(out))
    assert out == "stdout: í <- that was i with accute accent"
##    print()
##    print("repr(err):", repr(err))
    assert err == "ValueError: stderr: í <- that was i with accute accent"

# (win xp) passes in py 3.3.1 running both by if __name__ or pytest
# fails in 2.6.6 in both run modes
# seems that in 2.6.6 subprocess communicate forces the 'ascii' encoding
# for print(), 
def test_encoding_latin1():
    cmdline = ["cmd_timeout_proxy_encoding.py"]
    killed, returncode, err, out = cmd(cmdline)
    out = out.decode("latin1")
    out = strip_line_ending(out)
    err = err.decode("latin1")
    err = strip_line_ending(err)
##    print()
##    print("repr(err):", repr(err))
##    print()
##    print("repr(out):", repr(out))
    assert out == "stdout: í <- that was i with accute accent"
##    print("repr(err):", repr(err))
    assert err.endswith("ValueError: stderr: í <- that was i with accute accent")

if __name__ == "__main__":
#    test_encoding_latin1()
    try:
        test_encoding_latin1()
        print(">>> test_encoding_latin1 pass.\n")
    except Exception:
        print(">>> test_encoding_latin1 fails.\n")

#    test_encoding__sys_std_encoding()
    try:
        test_encoding__sys_std_encoding()
        print(">>> test_encoding__sys_std_encoding pass.\n")
    except Exception:
        print(">>> test_encoding__sys_std_encoding fails.\n")
