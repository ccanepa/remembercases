# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals

import sys
import time


def main(args):
    out = ','.join([s.__repr__() for s in args])
    sys.stdout.write('(fake)Traceback(fake) - dummy proxy args:\n@%s' % out)
    if len(args) > 1 and (args[1] == 'runforever'):
        while 1:
            time.sleep(1.0)


if __name__ == '__main__':
    main(sys.argv[1:])
