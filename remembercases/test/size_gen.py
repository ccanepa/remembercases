#!/usr/bin/env python
"""usado por cmd_timeout.py
Envia a stdout int(sys.argv[1]) bytes, con el patron
'B> b...b <E'
Luego, si len(sys.argv)==2, el script termina alli.
En caso contrario, entra en un loop infinito de NOP
"""
import sys
import time

def main():
    prefix = 'B> '
    tail = ' <E'
    output = prefix + "b" * (int(sys.argv[1])-(len(prefix)+len(tail))) + tail
    assert len(output) == int(sys.argv[1])
    sys.stdout.write(output)

    if len(sys.argv) > 2:
        while 1:
            time.sleep(1)


if __name__ == "__main__":
    main()
