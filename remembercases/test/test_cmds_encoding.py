# -*- coding: utf-8 -*-
"""tests cmd_timeout will not traceback even if encoding does not match, and
the repr of stdout and stderr are captured
"""

from __future__ import division, print_function, unicode_literals
import six

from remembercases.cmds import run_script


def test_matching_ascii_encoding():
    cmdline = ["h_cmd_timeout_encoding_test_helper.py", "ascii"]
    killed, returncode, err, out = run_script(cmdline)
    assert type(err) == six.text_type
    assert type(out) == six.text_type
    assert out == "stdout: All ascii characters"
    assert err == "stderr: All ascii characters"


def test_target_uses_latin1_listener_uses_ascii():
    # the point is to not traceback and store the info such that it could be
    # reinterpreted at a later time
    cmdline = ["h_cmd_timeout_encoding_test_helper.py", "latin1"]
    killed, returncode, err, out = run_script(cmdline, encoding="ascii")
    assert type(err) == six.text_type
    assert type(out) == six.text_type
    if six.PY3:
        # remove extra 'b' in py3
        out_z = out.replace("b", "", 1)
        err_z = err.replace("b", "", 1)
    else:
        out_z = out
        err_z = err
    assert out_z.startswith("repr: 'stdout: i_with_acute_accent ->")
    assert out_z.endswith("< tail'")
    assert err_z.startswith("repr: 'stderr: i_with_acute_accent ->")
    assert err_z.endswith("< tail'")


if __name__ == "__main__":
    try:
        test_matching_ascii_encoding()
        print(">>> test_matching_ascii_encoding PASS.\n")
    except Exception:
        print(">>> test_matching_ascii_encoding FAILS.\n")

    try:
        test_target_uses_latin1_listener_uses_ascii()
        print(">>> test_target_uses_latin1_listener_uses_ascii PASS.\n")
    except Exception as ex:
        print(">>> test_target_uses_latin1_listener_uses_ascii FAILS.\n")
        print(ex)
