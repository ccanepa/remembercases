# -*- coding: utf-8 -*-
"""
helper for test_cmd_timeout.py
"""

from __future__ import division, print_function, unicode_literals

import sys

def main():
    if len(sys.argv) != 2:
        raise Exception("Number of arguments must be 1")
    else:
        charset = sys.argv[1]
        assert charset in ["ascii", "latin1", "non_latin1"]
    if charset == "ascii":
        print("stdout: All ascii characters")
        raise ValueError("stderr: All ascii characters")
    elif charset == "latin1":
        # 'i with acute accent' is an interesting latin1 character because
        # is not a valid 'utf-8' encoding
        print("stdout: latin1 'i with acute accent' ->í<")
        raise ValueError("stderr: latin1 'i with acute accent' ->í<")
    elif charset == "non_latin1":
        # latin_small_letter_AE_with_acute, 
        ae = u'\u01fd'
        print("stdout: ascii characters plus non latin1 'LATIN SMALL LETTER AE WITH ACUTE' ->" + ae + "<")
        raise ValueError("stderr: ascii characters plus non latin1 'LATIN SMALL LETTER AE WITH ACUTE' ->" + ae + "<")
      
if __name__ == "__main__":
    main()
