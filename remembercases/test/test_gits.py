# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals
import six  # noqa: F401

import os
import shutil
import stat

import remembercases.gits as gi


# helper for shutil.rmtree, https://stackoverflow.com/questions/1889597/deleting-directory-in-python/1889686#1889686 # noqa E501
def remove_readonly(func, path, excinfo):
    os.chmod(path, stat.S_IWRITE)
    func(path)


def write_file(fname, text):
    asbytes = text.encode(encoding="utf8")
    with open(fname, "wb") as f:
        f.write(asbytes)


def payload(test_dir):
    # setup initial git repo, empty
    try:
        cmdline = ["git", "init"]
        gi.cmd_run_ok(cmdline, test_dir)
    except Exception:
        print("\nERROR: cannot git init, no test has run\n")
        raise

    # -> for gitlab-ci, in a raspi3 with raspian buster was not needed
    cmdline = ["git", "config", "user.email", "joe@example.com"]
    gi.cmd_run_ok(cmdline, test_dir)
    cmdline = ["git", "config", "user.name", "joe_blow"]
    gi.cmd_run_ok(cmdline, test_dir)
    # <- for gitlab-ci, in a raspi3 with raspian buster was not needed

    # add a file
    fname_1 = os.path.join(test_dir, "first.txt")
    write_file(fname_1, u"initial content")
    cmdline = ["git", "add", "."]
    gi.cmd_run_ok(cmdline, test_dir)
    cmdline = ["git", "commit", "-m", '"first commit"']
    gi.cmd_run_ok(cmdline, test_dir)

    commit_hash = gi.WD_hash(test_dir)
    commit_short_hash = gi.WD_short_hash(test_dir)
    assert commit_hash.startswith(commit_short_hash)

    assert gi.is_clean(test_dir)
    # changed files are detected
    write_file(fname_1, u"--modified--")
    assert not gi.is_clean(test_dir)

    # clean works with changed files
    gi.clean_WD(test_dir)
    assert gi.is_clean(test_dir)

    # unversioned files are detected
    fname_2 = os.path.join(test_dir, "unversioned.txt")
    write_file(fname_1, u"ysysy")
    assert not gi.is_clean(test_dir)

    # untracked files are deleted with clean_WD
    gi.clean_WD(test_dir)
    assert gi.is_clean(test_dir)

    # deleted files are detected
    os.unlink(fname_1)
    assert not gi.is_clean(test_dir)

    # deleted files are recovered with clean_WD
    gi.clean_WD(test_dir)
    assert gi.is_clean(test_dir)

    # modify first.txt, add second.txt, commit
    write_file(fname_1, u"--modified--")
    fname_2 = os.path.join(test_dir, "second.txt")
    write_file(fname_2, u"initial content 2nd file")
    cmdline = ["git", "add", "."]
    gi.cmd_run_ok(cmdline, test_dir)
    cmdline = ["git", "commit", "-m", '"second commit"']
    gi.cmd_run_ok(cmdline, test_dir)
    commit_short_hash_2 = gi.WD_short_hash(test_dir)
    assert gi.is_clean(test_dir)

    # checkout first commit
    gi.checkout(test_dir, commit_short_hash)
    assert gi.WD_short_hash(test_dir) == commit_short_hash
    assert gi.is_clean(test_dir)

    # checkout second commit using gversion="master"
    gi.checkout(test_dir, "master")
    assert gi.WD_short_hash(test_dir) == commit_short_hash_2

    # modify a tracked file and checkout other commit
    fname_2 = os.path.join(test_dir, "second.txt")
    write_file(fname_2, u"modified 2nd file")
    gi.checkout(test_dir, commit_short_hash)
    assert gi.WD_short_hash(test_dir) == commit_short_hash
    assert gi.is_clean(test_dir)

    # checkout second commit using gversion="master"
    gi.checkout(test_dir, "master")
    assert gi.WD_short_hash(test_dir) == commit_short_hash_2
    # make a local branch, modify WD and commit
    cmdline = ["git", "checkout", "-b", "brancho"]
    gi.cmd_run_ok(cmdline, test_dir)
    fname_2 = os.path.join(test_dir, "in_branch.txt")
    write_file(fname_2, u"initial content in_branch")
    cmdline = ["git", "add", "."]
    gi.cmd_run_ok(cmdline, test_dir)
    cmdline = ["git", "commit", "-m", '"second commit"']
    # commit_short_hash_branch = gi.WD_short_hash(test_dir)

    # change branch by checking first repo commit
    gi.checkout(test_dir, commit_short_hash)
    assert gi.WD_short_hash(test_dir) == commit_short_hash
    assert gi.is_clean(test_dir)

    # checkout a prev version, modify, see that the version
    # does not changes after clean
    gi.checkout(test_dir, commit_short_hash)
    os.unlink(fname_1)
    gi.clean_WD(test_dir)
    assert gi.WD_short_hash(test_dir) == commit_short_hash


def test():
    # make tmpdir for testing
    test_dir = os.path.abspath("tmp")
    try:
        shutil.rmtree(test_dir, onerror=remove_readonly)
    except Exception as ex:
        print(ex)
        pass
    os.mkdir(test_dir)
##    try:
    payload(test_dir)
##    except Exception as ex:
##        print("Exception:", ex)
##        pass
    print("*** Done running tests ***")
    # works in win10 + py37
    # shutil.rmtree(test_dir, onerror=remove_readonly)


if __name__ == "__main__":
    test()
