# -*- coding: utf-8 -*-
"""explore parametrized remembercases.cmd_timeout.run_script behavior related to
    - timeout compliance
    - full capture of stdout - stderr under timeout, traceback and normal termination
"""
from __future__ import division, print_function, unicode_literals

import remembercases.cmds as cm

import time


# compatibility 23
def function_name(f):
    """returns the function name"""
    try:
        name = f.__name__
    except Exception:
        name = f.func_name
    return name


# >>>>> nothing interesting, only report formatting
def justify(s, width):
    return ' ' * (width - len(s)) + s


class FormatterLineReport(object):
    """
    Tool to format a data table with headers, assumes monospaced font.

    Usage:
        Create
        Add the desired rows with .add_header
        Get the headers line using .headers_string
        Get each row line with .line_string
        Done
    """

    def __init__(self):
        self.order = []  # header
        self.all_justified_headers = {}  # header: justified_header
        self.all_widths = {}  # header: column_width

    def add_header(self, header, max_data_width):
        """
        Adds a column with header 'header' and declares that data in this
        column will not need more than 'max_dat_width' to display
        """
        column_width = max(len(header), max_data_width) + 1
        justified_header = justify(header, column_width)
        assert len(justified_header) == column_width
        self.order.append(header)
        self.all_justified_headers[header] = justified_header
        self.all_widths[header] = column_width

    def headers_string(self):
        """
        returns a string with all headers, each one justified to a width that
            1. at least hold header plus a space
            2. holds the max_data_width declared for the header plus one space
        Order is the same as headers were added.
        """
        jheaders = self.all_justified_headers
        return ''.join([jheaders[header] for header in self.order])

    def line_string(self, sdict):
        """
        receives a dict with column_name: str_value pairs, returns a string that
        nicely aligns with headers_string"""
        parts = [justify(sdict[name], self.all_widths[name])
                 for name in self.order]
        return ''.join(parts)


def get_formatter(funcs, nbytes, bufsizes):
    formatter = FormatterLineReport()
    formatter.add_header('func', max([len(function_name(f)) for f in funcs]))
    # formatter.add_header('bufsize', max([len(s) for s in bufsizes]))
    formatter.add_header('sent', max([len(s) for s in nbytes]))
    formatter.add_header('received', max([len(s) for s in nbytes]))
    formatter.add_header('match', 0)
    formatter.add_header('partial_match', 0)
    formatter.add_header('elapsed', 6)
    formatter.add_header('timeout', 0)
    formatter.add_header('killed', 6)
    # formatter.add_header('retcode', 0)
    return formatter


# <<< end formatter stuff


def test_for_sizes():
    import itertools as ito
    implementations = [cm.run_script, ]
    nbytes = ['4095', '4096', '4097', str(2 ** 13 - 1), str(2 ** 13), str(2 ** 13 + 1), str(2 ** 20 - 1), str(2 ** 20)]  # noqa: E501
    # -1==system default, 0==unbuffered, 1==line buffered, >1==aprox bufsize
    # bufsizes = ['-1', '0', '1', '80000']
    # in win xp, python 2.6.6, no effect in blocking or bytes transferred, so use -1
    bufsizes = ['-1']
    timeout = 3

    print('\ntest for sizes\n')
    formatter = get_formatter(implementations, nbytes, bufsizes)
    print(formatter.headers_string())

    for bufsize, size, func in ito.product(bufsizes, nbytes, implementations):
        t_begin = time.time()
        seconds_passed = 0
        cmdline = ['h_size_gen.py', size]
        killed, rc, err, output = func(cmdline, bufsize=int(bufsize), timeout=timeout)
        seconds_passed = time.time() - t_begin

        # in sync with size_gen.py
        prefix = 'B> '
        tail = ' <E'
        expect = prefix + "b" * (int(size) - (len(prefix) + len(tail))) + tail

        dstr = {
            'func': function_name(func),
            # 'bufsize': bufsize,
            'sent': size,
            'received': str(len(output)),
            'match': (output == expect).__repr__(),
            'partial_match': expect.startswith(output).__repr__(),
            'elapsed': "%5.3f" % seconds_passed,
            'timeout': "%d" % timeout,
            'killed': killed.__repr__(),
            # 'retcode': "%s" % rc
        }
        print(formatter.line_string(dstr))
        if err:
            print('received stderr', err)
            print()


def test_for_timeout():
    # same code as before, except an extra param is passed to size_gen.py
    # so after pumping stdout it will wait forever without quiting.

    import itertools as ito
    implementations = [cm.run_script, ]
    nbytes = ['4095', '4096', '4097', str(2 ** 13 - 1), str(2 ** 13), str(2 ** 13 + 1), str(2 ** 20 - 1), str(2 ** 20)]  # noqa: E501
    # -1==system default, 0==unbuffered, 1==line buffered, >1==aprox bufsize
    # bufsizes = ['-1', '0', '1', '80000']
    # in win xp, python 2.6.6, no effect in blocking or bytes transferred, so use -1
    bufsizes = ['-1']
    timeout = 3

    print('\ntest for timeout\n')
    formatter = get_formatter(implementations, nbytes, bufsizes)
    print(formatter.headers_string())

    for bufsize, size, func in ito.product(bufsizes, nbytes, implementations):
        t_begin = time.time()
        seconds_passed = 0
        cmdline = ['h_size_gen.py', size, '1']
        killed, rc, err, output = func(cmdline, bufsize=int(bufsize), timeout=timeout)
        seconds_passed = time.time() - t_begin

        # in sync with size_gen.py
        prefix = 'B> '
        tail = ' <E'
        expect = prefix + "b" * (int(size) - (len(prefix) + len(tail))) + tail

        dstr = {
            'func': function_name(func),
            # 'bufsize': bufsize,
            'sent': size,
            'received': str(len(output)),
            'match': (output == expect).__repr__(),
            'partial_match': expect.startswith(output).__repr__(),
            'elapsed': "%5.3f" % seconds_passed,
            'timeout': "%d" % timeout,
            'killed': killed.__repr__(),
            # 'retcode': "%s" % rc
        }
        print(formatter.line_string(dstr))
        if err:
            print('received stderr', err)
            print()


if __name__ == "__main__":
    test_for_sizes()
    test_for_timeout()

#### sample output for win7, python 3.7.7
##    test for sizes
##
##           func    sent received match partial_match elapsed timeout killed
##     run_script    4095     4095  True          True   0.045       3  False
##     run_script    4096     4096  True          True   0.040       3  False
##     run_script    4097     4097  True          True   0.038       3  False
##     run_script    8191     8191  True          True   0.038       3  False
##     run_script    8192     8192  True          True   0.037       3  False
##     run_script    8193     8193  True          True   0.038       3  False
##     run_script 1048575  1048575  True          True   0.047       3  False
##     run_script 1048576  1048576  True          True   0.047       3  False
##
##    test for timeout
##
##           func    sent received match partial_match elapsed timeout killed
##     run_script    4095     4095  True          True   3.013       3   True
##     run_script    4096     4096  True          True   3.008       3   True
##     run_script    4097     4097  True          True   3.007       3   True
##     run_script    8191     8191  True          True   3.014       3   True
##     run_script    8192     8192  True          True   3.009       3   True
##     run_script    8193     8193  True          True   3.011       3   True
##     run_script 1048575  1048575  True          True   3.009       3   True
##     run_script 1048576  1048576  True          True   3.014       3   True


#### sample output for win7, python 2.7.18

##    test for sizes
##
##           func    sent received match partial_match elapsed timeout killed
##     run_script    4095     4095  True          True   0.027       3  False
##     run_script    4096     4096  True          True   0.026       3  False
##     run_script    4097     4097  True          True   0.018       3  False
##     run_script    8191     8191  True          True   0.017       3  False
##     run_script    8192     8192  True          True   0.018       3  False
##     run_script    8193     8193  True          True   0.017       3  False
##     run_script 1048575  1048575  True          True   0.020       3  False
##     run_script 1048576  1048576  True          True   0.022       3  False
##
##    test for timeout
##
##           func    sent received match partial_match elapsed timeout killed
##     run_script    4095     4095  True          True   3.003       3   True
##     run_script    4096     4096  True          True   3.003       3   True
##     run_script    4097     4097  True          True   3.003       3   True
##     run_script    8191     8191  True          True   3.003       3   True
##     run_script    8192     8192  True          True   3.003       3   True
##     run_script    8193     8193  True          True   3.002       3   True
##     run_script 1048575  1048575  True          True   3.004       3   True
##     run_script 1048576  1048576  True          True   3.004       3   True
