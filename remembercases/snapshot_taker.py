# -*- coding: utf-8 -*-
"""
testinfo
--------

Testinfo is embedded in a script into a single line

testinfo = "..."

The string delimiters are exactly double quotes, the line can't be continued
nor have extra text after the closing double quote.
Double quotes can't be present in the inner part, even if escaped.

A valid <testinfo string> is a concatenation of valid <parametrized commands>,
separated by ','

A <valid parametrized command> is a concatenation of a <known command> and
one or more <parameters>, separated by spaces.
To be valid, the quantity and type of parameters must match the ones declared
in known_commands.

int and float parameters use the stock python syntax.

strings are writen without delimiters and can't contain spaces.
They are used to fed key events, and theres a substitution table to express
the few keys needed to drive the tests.
"""
from __future__ import division, print_function, unicode_literals

import os


def qstr(s):
    s = str(s)
    s = s.replace('^', ' ')
    return s


def ustr(s):
    """ unfiltered string"""
    return s


class _ValidationHelper(object):
    def __init__(self):
        self.running = True
        self.expected_snapshots = []
        self.keys_dispatched = []

    def fn_quit(self):
        self.running = False

    def fn_take_snapshot(self, fname):
        self.expected_snapshots.append(fname)


class ScreenSampler(object):
    """
    Drives an app trough a series of well defined states and takes snapshots.

    The states are specified in a testinfo string, which is broken in a sequence
    of commands to perform.

    The public API is
        validated_info
        sampler
        next
        diagnostic

    """
    command_descriptions = {
        # <cmd>: list of str_to_argtype callables to convert args
        # callables should raise ValueError if conversion not possible.
        "s": [],  # take snapshot; snapshot name is implicit and should not be
                  # provided, will be something as <test_name><pyglet time>
        "t": [float],  # set time to param0, of type float, in seconds
        "dt": [float],  # increase time by param0, of type float, in seconds
        "q": [],  # cleanly quit app
        "f": [int, float],  # repeat param0 times: 'dt param1'
        }

    AUTO_DT = 0.033  # aprox 1/30

    @classmethod
    def validated_info(cls, testinfo, script_basename):
        diagnostic, expected_snapshots, obj = cls._parse(testinfo, script_basename)
        return diagnostic, expected_snapshots

    @classmethod
    def sampler(cls, testinfo, script_basename,
                fn_quit, fn_take_snapshot, snapshots_dir=None):
        """
        fn_quit: function
            cleanly terminates the script; should be called without parameters
        fn_take_snapshot: function
            will be called like fn_take_snapshot(fname). Should capture the
            screen and store in a file named fname.
        """
        obj, diagnostic = cls.new_from_testinfo(testinfo, script_basename)
        if not diagnostic:
            obj.prepare(fn_quit, fn_take_snapshot, snapshots_dir)
        return obj, diagnostic

    @classmethod
    def new_from_testinfo(cls, testinfo, script_basename):
        diagnostic, expected_snapshots, obj = cls._parse(testinfo, script_basename)
        if diagnostic:
            obj = None
        return obj, diagnostic

    @classmethod
    def _parse(cls, testinfo, script_basename):
        expected_snapshots = []
        obj = None
        all_fullcommands = [s.strip() for s in testinfo.split(',')]
        combo = [cls._parse_fullcommand(s, i) for i, s in enumerate(all_fullcommands)]
        diagnostic = '\n'.join([diag for diag, x in combo if diag])
        if diagnostic == '':
            # good, no errors while parsing
            # dry run the commands, validating timeflow and generating
            # expected_snapshots
            parsed_commands = [pc for diag, pc in combo]
            obj = cls(parsed_commands, script_basename)
            vh = _ValidationHelper()

            obj.prepare(vh.fn_quit, vh.fn_take_snapshot, snapshots_dir=None)
            external_app_time = 0.0
            while vh.running:
                external_app_time = obj.next(external_app_time)

            diagnostic = obj.diagnostic
            expected_snapshots = vh.expected_snapshots

        return diagnostic, expected_snapshots, obj

    @classmethod
    def _parse_fullcommand(cls, fullcommand, ordinal=0):
        """
        Parses and validates a string supposed to be a <parametrized command>.

        Parameters:
            fullcommand: str
                string to parse
            ordinal: int
                value to use while doing diagnostic messages, will be used as
                '... at cmd# %d' % ordinal
                default value is 0

        The different parts in the string to parse are separated by one or
        more spaces.

        To be valid, the cmd must match one of the entries in
        cls.command_descriptions, so it must match in command name, quantity
        and types of arguments.

        returns a pair diagnostic, parsed_command

        diagnostic: str
            '' if success, else a string with a description of errors found

        cmd_parts: list
            a list [ cmd_name, arg0,..., argn ], where the args converted as
            specified by cls.command_descriptions
        """
        parts = [p for p in fullcommand.split(' ') if p]
        cmd = parts[0]

        # verify it is a known command, it haves the correct number of
        # parameters, parameters can be translated to the correct type.
        diagnostic = ''
        if cmd in cls.command_descriptions:
            args_type = cls.command_descriptions[cmd]
            if len(args_type) != (len(parts)-1):
                msg = "Eti01 - at cmd# %d, cmd '%s' uses %d parameters, got %d"
                diagnostic = msg % (ordinal, cmd, len(args_type), len(parts)-1)
            else:
                for i, str_to_argtype in enumerate(args_type):
                    try:
                        # convert inplace
                        parts[i+1] = str_to_argtype(parts[i+1])
                    except ValueError:
                        if hasattr(str_to_argtype, 'func_name'):
                            typename = "%s" % str_to_argtype.func_name
                        else:
                            typename = "%s" % str_to_argtype
                        msg = ("Eti02 - at cmd# %d, cmd '%s' expects param %d" +
                               " of type %s, got %s")
                        diagnostic = msg % (ordinal, cmd, i, typename, parts[i+1])
                        break
        else:
            msg = "Eti05 - at cmd# %d, Unknown command '%s'"
            diagnostic = msg % (ordinal, cmd)

        parsed_command = parts
        return diagnostic, parsed_command

    def __init__(self, parsed_commands, script_basename):
        self.parsed_commands = parsed_commands
        s = script_basename
        # make template to generate snapshots basename from script basename and
        # app time at the snapshot
        prefix = s[:s.rfind('.py')] + '_'
        template = prefix + '%06.3f' + '.png'
        self.snapshot_filename_template = template
        self.diagnostic = ''
        self._reset()

    def _reset(self):
        self.prepared = False
        self.i_cmd = 0
        self.app_time = 0.0

    def prepare(self, fn_quit, fn_take_snapshot, snapshots_dir=None):
        # can't operate if parsing or validation errors were diagnosed
        assert self.diagnostic == ''
        self._reset()
        self.fn_quit = fn_quit
        self.fn_take_snapshot = fn_take_snapshot
        self.snapshots_dir = snapshots_dir
        self._repeat = False
        self.prepared = True

    def next(self, pyglet_app_time):
        if not self.prepared:
            raise Exception("'prepare' must be called before any call to 'next'")
        if pyglet_app_time is None:
            return 0.0

        # if other code is changing the pyglet app time the snapshots
        # timestamps would be misleading
        assert self.app_time == pyglet_app_time

        while self.app_time == pyglet_app_time:
            if self._repeat:
                cmd, args = self._repeat_cmd
            else:
                if self.i_cmd == len(self.parsed_commands):
                    # no more commands, assume implicit 'q' (quit)
                    cmd = 'q'
                    args = []
                else:
                    cmd = self.parsed_commands[self.i_cmd][0]
                    args = self.parsed_commands[self.i_cmd][1:]
                    self.i_cmd += 1

            getattr(self, 'cmd_' + cmd)(*args)
            if self.diagnostic:
                # have error, so force termination
                self.i_cmd = len(self.parsed_commands)

        return self.app_time

    def cmd_q(self):
        self.app_time += 0.1
        self.prepared = False
        self.fn_quit()

    def cmd_t(self, t):
        if t <= self.app_time:
            msg = "Eti03 - at cmd# %d, not ascending time: cmd 't' %f when time was %f"
            self.diagnostic = msg % (self.i_cmd, t, self.app_time)
        else:
            self.app_time = t

    def cmd_dt(self, dt):
        if dt <= 0:
            msg = "Eti04 - at cmd# %d, not ascending time: cmd 'dt' %f when time was %f"
            self.diagnostic = msg % (self.i_cmd, dt, self.app_time)
        else:
            self.app_time += dt

    def cmd_s(self):
        fname = self.snapshot_filename_template % self.app_time
        if self.snapshots_dir is not None:
            fname = os.path.join(self.snapshots_dir, fname)
        self.fn_take_snapshot(fname)

    def cmd_f(self, num_frames, dt):
        if num_frames < 1:
            msg = "Eti06 - at cmd# %d, count frames less than 1 st cmd 'f'"
            self.diagnostic = msg % self.i_cmd
        elif dt <= 0.0:
            msg = "Eti07 - at cmd# %d, not ascending time, cmd 'f' with 'dt'=%f"
            self.diagnostic = msg % (self.i_cmd, dt)
        else:
            self._repeat = True
            self._repeat_count = num_frames
            self._repeat_cmd = ('_rdt', [dt])

    def cmd__rdt(self, dt):
        self.app_time += dt
        self._repeat_count -= 1
        self._repeat = (self._repeat_count > 0)
