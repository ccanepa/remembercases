# -*- coding: utf-8 -*-
"""
Support to run a cmd in a subprocess, killing the subprocess if the
run time exceeds a timeout. Must run in Windows and *nix.

Variants

  - cmd_run
  stdout, stderr from the subprocess are captured, returncode is reported,
  also timeout status is reported.

  - cmd_run_ok
  raises CmdExecutionError if error or timeout, returns stdout

  - run_script
  runs python script in a subprocess, same args and returns as cmd_run

Support for timeout was added to python in 3.3, for 2.6 and 2.7 the code was
was inspired by
http://stackoverflow.com/questions/3575554/python-subprocess-with-timeout-and-large-output-64k
http://betabug.ch/blogs/ch-athens/1093

Thanks Daniel Moisset for the hint to use -u to allow capture all stdout. stderr
"""
from __future__ import division, print_function, unicode_literals
import six

import os
import subprocess
import threading
import sys


class CmdExecutionError(Exception):
    pass


def decode_communicate(bs, encoding='latin1'):
    """decoding for strings from stdout, stderr"""
    try:
        s = bs.decode(encoding)
    except UnicodeDecodeError:
        s = "repr: " + six.text_type(repr(bs))
    return s


def cmd_run__py3(cmdline, cwd=None, bufsize=0, encoding='latin1', timeout=60):
    """run a command, returns (killed_by_timeout, returncode, err, out)

    If the command can be run at all it should not raise any exception.

    Bad parameters can raise exceptions, by example a typo "git" -> "fit"
    will raise OSError, because the cmd "fit" will not be found.

    Parameters:
        cmdline: list
            as in the list style of subprocess.Popen, not the string style.
            the first element is the python script name, followed by
            args to be passed to the script.
            Example: ["git", "checkout", gversion, "--force"]

        cwd: string or None
            If cwd is not None, the child’s current directory will be
            changed to cwd before it is executed.

        bufsize: int
            as in subprocess.Popen:
            0 means unbuffered
            1 means line buffered
            n>1: buffer of at least n bytes
            n<-1: system default, which usually means fully buffered.
            The default value for bufsize is 0 (unbuffered).

        timeout: float
            time in seconds, if the subprocess takes more than timeout
            seconds to complete, it is killed

        encoding: str
            string specifying which encoding to use when translating stdout and
            stderr from bytes to unicode. If the conversion to text fails
            with a decode error, then 'out' will carry
                "repr: " + unicode(repr(stdout))
            Similar with stderr.

    Returns a tuple (killed, returncode, err, out) where
        killed: bool
            True if subprocess killed after exceeding timeout
        returncode: int
            as in subprocess, except is 1 if timeout hit
        err: unicode
            stderr from the subprocess (but see 'encoding' above)
        out: unicode
            stdout from the subprocess (but see 'encoding' above)
    """
    p = subprocess.Popen(
        cmdline,
        bufsize=bufsize,
        shell=False,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        cwd=cwd
    )
    killed = True
    try:
        out, err = p.communicate(timeout=timeout)
        killed = False
    except subprocess.TimeoutExpired:
        p.kill()
        out, err = p.communicate()

    out = decode_communicate(out, encoding)
    err = decode_communicate(err, encoding)
    returncode = p.returncode

    return killed, returncode, err, out


def cmd_run__py2(cmdline, cwd=None, bufsize=0, encoding='latin1', timeout=60):
    """run a command, returns (killed_by_timeout, returncode, err, out)

    Needs at least python 2.6 (for subprocess.Popen.kill)

    If the command can be run at all it should not raise any exception.

    Bad parameters can raise exceptions, by example a typo "git" -> "fit"
    will raise OSError, because the cmd "fit" will not be found.

    Parameters:
        cmdline: list
            as in the list style of subprocess.Popen, not the string style.
            the first element is the python script name, followed by
            args to be passed to the script.
            Example: ["git", "checkout", gversion, "--force"]

        cwd: string or None
            If cwd is not None, the child’s current directory will be
            changed to cwd before it is executed.

        bufsize: int
            as in subprocess.Popen:
            0 means unbuffered
            1 means line buffered
            n>1: buffer of at least n bytes
            n<-1: system default, which usually means fully buffered.
            The default value for bufsize is 0 (unbuffered).

        timeout: float
            time in seconds, if the subprocess takes more than timeout
            seconds to complete, it is killed

        encoding: str
            string specifying which encoding to use when translating stdout and
            stderr from bytes to unicode. If the conversion to text fails
            with a decode error, then 'out' will carry
                "repr: " + unicode(repr(stdout))
            Similar with stderr.

    Returns a tuple (killed, returncode, err, out) where
        killed: bool
            True if subprocess killed after exceeding timeout
        returncode: int
            as in subprocess, except is 1 if timeout hit
        err: unicode
            stderr from the subprocess (but see 'encoding' above)
        out: unicode
            stdout from the subprocess (but see 'encoding' above)
    """
    kill_check = threading.Event()

    def _kill_after_timeout(subproc):
        subproc.kill()
        kill_check.set()  # tell the main routine that we had to kill

    p = subprocess.Popen(
        cmdline,
        bufsize=bufsize,
        shell=False,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        cwd=cwd
    )
    watchdog = threading.Timer(timeout, _kill_after_timeout, args=(p,))
    watchdog.start()

    out, err = p.communicate()

    watchdog.cancel()  # if it's still waiting to run
    killed = kill_check.isSet()
    kill_check.clear()

    out = decode_communicate(out, encoding)
    err = decode_communicate(err, encoding)
    returncode = p.returncode

    return killed, returncode, err, out


if six.PY2:
    cmd_run = cmd_run__py2
else:
    cmd_run = cmd_run__py3


def cmd_run_ok(cmdline, cwd=None, bufsize=0, encoding='latin1', timeout=60):
    """run command, returns stdout or raises CmdExecutionError on error

    Raises CmdExecutionError if returncode != 0 or timeout reached

    Bad parameters can raise additional exceptions, by example a typo
    "git" -> "fit" will raise OSError, because the cmd "fit" will not be found.

    The cmd output is decoded with the specified 'encoding', else it tries to
    decode as 'latin1'; if that fails returns  "repr: " + repr(out)

    Some software may emmit to stderr even if returncode is zero
    (like git checkout). question: should be a flag to return stderr + stdout ?
    """
    killed, returncode, err, out = cmd_run(cmdline, cwd=cwd, bufsize=bufsize,
                                           encoding=encoding, timeout=timeout)
    not_ok = killed or returncode
    ok = not not_ok
    if ok:
        return out

    cmdstr = " ".join(cmdline)
    parts = ["Error while running cmd\n", "    " + cmdstr, "details:"]
    parts.append("cwd: %s" % cwd)
    parts.append("timeout: %s" % killed)
    parts.append("returncode: %s" % returncode)
    parts.append("stderr: %s" % err)
    parts.append("stdout: %s" % out)
    parts.append("------------------\n")
    diagnostic = "\n".join(parts)
    raise CmdExecutionError(diagnostic)


def run_script(scriptline, py_cmd=None, cwd=None, bufsize=0, encoding='latin1', timeout=60):
    """Runs a python script in a subprocess, killing the subprocess if the
    run time exceeds a specified timeout. returns (killed, returncode, err, out)

    py_cmd: python executable to run the script
      by example ["venv_path/Scripts/python",] or None to use the same python
      as the calling code

    Parameters the same as in cmd_run__py3, with 'scriptline' starting with
    flags to the python interpreter followed by the filename for the script
    to run, then the flags and args for the script. Example::
        scriptline = ['-Wd', 'myscript.py', '-v']

    The '-u' for unbuffered, necessary to fully capture output on traceback is
    always added.

    Remember to pass '-Wd' if deprecation warnings are desired.

    Other params and returns same as in cmd_run__py3
    """
    if py_cmd is None:
        py_cmd = [sys.executable, ]
    cmdline = list(py_cmd) + ["-u"] + scriptline
    return cmd_run(cmdline, cwd=cwd, bufsize=bufsize,
                   encoding=encoding, timeout=timeout)


def run_script_proxied_simple(proxy_abspath, target_script, proxy_args,
                              py_cmd=None, cwd=None, bufsize=0, encoding='latin1', timeout=60):
    """Runs a proxy python script in a subprocess, which will in turn exercises
       a python target_script, returns (killed, returncode, err, out)

    The proxy cares about monkey-patching, params, sequencing and Exception
    reporting for target_script.

    Think of it as
      - target_script: small demo code
      - proxy: test driver that exercises the demo

    The common pattern is that proxy imports target_script, does a setup, then
    calls the target_script's main in a try-catch block.
    """
    if cwd is None:
        # set cwd to the dir 'target_script' lives
        cwd = os.path.abspath(target_script)
    scriptline = [proxy_abspath, target_script] + proxy_args
    return run_script(scriptline, py_cmd=py_cmd, cwd=cwd, bufsize=bufsize,
                      encoding=encoding, timeout=timeout)


def run_script_proxied_custom(proxy_abspath, target_script, proxy_args,
                              py_cmd=None, cwd=None, bufsize=0, encoding='latin1', timeout=60):
    if cwd is None:
        # set cwd to the dir 'target_script' lives
        cwd, target_shortname = os.path.split(os.path.abspath(target_script))
    scriptline = [proxy_abspath, target_shortname] + proxy_args
    if py_cmd is None:
        py_cmd = [sys.executable, ]
    cmdline = list(py_cmd) + ["-u"] + scriptline
    print('proxy run cmdline:', cmdline)
    killed, rc, err, output = cmd_run(cmdline, cwd=cwd, bufsize=bufsize,
                                      encoding=encoding, timeout=timeout)
    traceback_in_stdout = 'Traceback' in output
    if traceback_in_stdout:
        err += ('\nstdout with traceback:\n' + output)

    return killed, err

# note to me:
#    Some example code called subprocess.Popen with
#        cmdline = ['exp_gen.py', size]
#        shell = False
#    With this, in win xp, python 2.6.6 I got a traceback in subprocess.py:
#        WindowsError: [Error 193] %1 not a valid Win32 app
#
#    Two ways to get rid off that were
#        cmdline = [sys.executable, 'exp_gen.py', size]
#        shell = False
#    or
#        cmdline = ['exp_gen.py', size]
#        shell = True
#    The later is not exactly equivalent, if I understood well the p.pid here
#    would be the one of the shell, not the one of spawned python process.
