from __future__ import division, print_function, unicode_literals
import six

import os

if six.PY2:
    from .cmd_timeout import cmd__uses_event as cmd
else:
    from .cmd_timeout import cmd__py3 as cmd

def proxy_run(proxy_abspath, test_fname, proxy_args, timeout=5.0):
    """
    proxy_abspath: absolute path to script that will run the test
    test_fname: path to script to test
    timeout: the test run will be killed after this time elapses.
    """

    cwd_wanted, basename = os.path.split(test_fname)
    if cwd_wanted == '':
        cwd_wanted = '.'
    
    cmdline = [proxy_abspath, basename]
    cmdline.extend(proxy_args)
    print('proxy run cmdline:', cmdline)
    killed, rc, err, output = cmd(cmdline, cwd=cwd_wanted, timeout=timeout)

    traceback_in_stdout = 'Traceback' in output
    if traceback_in_stdout:
        err += ('\nstdout with traceback:\n' + output) 

    return killed, err
