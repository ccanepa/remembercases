# -*- coding: utf-8 -*-
"""operations with git"""

from __future__ import division, print_function, unicode_literals
import six  # noqa: F401

from remembercases.cmds import cmd_run_ok


def is_clean(path):
    cmdline = ["git", "status", "--porcelain"]
    out = cmd_run_ok(cmdline, path)
    return out == ""


def clean_WD(path):
    # delete untracked files
    cmdline = ["git", "clean", "-d", "-x", "-f"]
    cmd_run_ok(cmdline, path)
    # reverts editions - deletions of tracked files
    # git restore was introduced in v2.23 - In raspbian buster
    # only v2.20.1 is available with apt-get. Udr git reset?
    # cmdline = ["git", "restore", "."]

    cmdline = ["git", "checkout", ".", "-f"]

    cmd_run_ok(cmdline, path)


def WD_short_hash(path):
    cmdline = ['git', 'log', '-1', '--format="%h"']
    out = cmd_run_ok(cmdline, path).strip()
    if out.startswith('"'):
        out = out[1:-1]
    return out


def WD_hash(path):
    cmdline = ['git', 'log', '-1', '--format="%H"']
    out = cmd_run_ok(cmdline, path).strip()
    if out.startswith('"'):
        out = out[1:-1]
    return out


def checkout(path, gversion):
    print("gits.checkout - path:", path)
    cmdline = ["git", "checkout", gversion, "--force"]
    cmd_run_ok(cmdline, path)


def checkout_file(WD_path, checkout_str, fname):
    """checkouts fname at the version checkout_str

    fname absolute or relative to WD_path
    """
    cmdline = ['git', 'checkout', checkout_str, fname, fname]
    print("gits.checkout_file - cmdline:", " ".join(cmdline))
    cmd_run_ok(cmdline, cwd=WD_path)
