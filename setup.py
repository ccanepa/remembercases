# -*- coding: utf-8 -*-
"""setup -- setuptools setup file for remembercases

Supports the usual 'setup.py install' for remembercases.
"""

__author__ = "ccanepa"
__author_email__ = "ccanepacc@gmail.com"
__version__ = "0.3"
__date__ = "2019 08 11 +"

try:
    import setuptools
except ImportError:
    from ez_setup import use_setuptools
    use_setuptools()

from setuptools import setup, find_packages

import os

f = open('README', 'rU')
long_description = f.read()
f.close()

install_requires=['six>=1.4', ]
dependency_links = []

setup(
    name = "remembercases",
    version = __version__,
    author = __author__,
    license="BSD",
    description = "runs, record and compare results of running probe scripts",
    long_description=long_description,
    url = "https://bitbucket.org/ccanepa/remembercases",
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Environment :: MacOS X",
        "Environment :: Win32 (MS Windows)",
        "Environment :: X11 Applications",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Natural Language :: English",
        "Operating System :: MacOS :: MacOS X",
        "Operating System :: Microsoft :: Windows",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 3",
        ("Topic :: Software Development :: Libraries :: Python Modules"),
        #("Topic :: Games/Entertainment"),
        ],
 
    packages = ["remembercases"],
    package_data={},

    install_requires=install_requires,
    dependency_links=dependency_links,

    include_package_data = True,
    zip_safe = False,
    )
