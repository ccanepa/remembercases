# -*- coding: utf-8 -*-
"""setup -- setuptools setup file for remembercases

Supports the usual 'setup.py install' for remembercases.
"""

__author__ = "Claudio Canepa"
__author_email__ = "ccanepacc@gmail.com"
__version__ = "0.4.2"
__date__ = "2020 11 24"

from setuptools import setup

with open('README.rst', 'rb') as f:
    as_bytes = f.read()
long_description = as_bytes.decode("utf8")

install_requires = ['six>=1.4', ]
dependency_links = []

setup(
    name="remembercases",
    version=__version__,
    author=__author__,
    author_email=__author_email__,
    license="BSD",
    description="runs, record and compare results of running probe scripts",
    long_description=long_description,
    url="https://gitlab.com/ccanepa/remembercases",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: MacOS X",
        "Environment :: Win32 (MS Windows)",
        "Environment :: X11 Applications",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Natural Language :: English",
        "Operating System :: MacOS :: MacOS X",
        "Operating System :: Microsoft :: Windows",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 3",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Software Development :: Testing",
        ],

    packages=["remembercases"],
    package_data={},

    install_requires=install_requires,
    dependency_links=dependency_links,

    include_package_data=True,
    zip_safe=False,
    )
